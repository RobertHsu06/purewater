<?php
    error_reporting(E_ALL);
    ini_set('display_errors',1);
# echo "test";
// $userInfo = posix_getpwuid(posix_getuid());
// $user = $userInfo['name'];
// echo $user;

// $groupInfo = posix_getgrgid(posix_getgid());
// $group = $groupInfo = $groupInfo['name'];
// echo $group;
// echo 'Current script owner: ' . get_current_user();

$file_db = NULL;
$postParm = $_POST['parm'];
$projectMedia_name = "";
$projectMediaPath = "/purewater/projectMedia/";
$uploaddir = '/var/www/html';
 // [personalImage] => Array
 //        (
 //            [name] => waterImage.jpg
 //            [type] => image/jpeg
 //            [tmp_name] => /tmp/php0sdHU8
 //            [error] => 0
 //            [size] => 136157
 //        )

 //    [IDImage] => Array
 //        (
 //            [name] => 擷取.JPG
 //            [type] => image/jpeg
 //            [tmp_name] => /tmp/phpwUcY1D
 //            [error] => 0
 //            [size] => 90813
 //        )


// /
        // echo $postParm;
        //create sqlite db if not existed

        //parm={name:xxxx, password:xxxxx, DoB:xxx, email:xxx, address:xxx, telephone:xxx, role:xxx(inventor,investor,donator,charity) website:xx(optional), }

    try{

        $file_db = new PDO('sqlite:purewater.sqlite');
//echo "step1";
               	//parm={name:xxxx, password:xxxxx}
                if($postParm)
                {

                        $objPostParm = json_decode($postParm);
            $checkResult = check_parameter($objPostParm);

                        if(strlen($checkResult) > 0){
                                echo $checkResult;
                                return;
                        }

                        //  //check account exist
                        // $sth=$file_db->prepare("select count(*) from role_table where name='" .$objPostParm->{'name'} ."'" . " AND role='" .$objPostParm->{'role'} ."'" );

                        // $sth->execute();
                        // $rowCount=$sth->fetchColumn(); //取得欄位1 的值  (也就是count(*))

                        // //echo $postParm;
                        // if(intval($rowCount) > 0)
                        // {
                        //         echo "{result:-100,text:account existed}";
                        // }
                        // else
                        {
                                if (count($_FILES) > 0) {
                                    if(isset($_FILES["projectMedia"])){
                                        if($_FILES["projectMedia"]["error"] > 0){
                                            print_r($_FILES["projectMedia"]["error"]);
                                            echo "{result:-201,text:projectMedia updload failed}";
                                            return;
                                        }
                                        // print_r($_FILES["personalImage"]);
                                        $projectMedia_name = uniqid() . "_" . $_FILES["projectMedia"]["name"];

                                        $uploadfile = $uploaddir  . $projectMediaPath . $projectMedia_name;

                                        if (move_uploaded_file($_FILES['projectMedia']['tmp_name'], $uploadfile)) {
                                            $projectMedia_name = $projectMediaPath . $projectMedia_name;
                                        } else {
                                            echo "{result:-202,text:project media updload failed}";
                                            return;
                                        }
                                    } 
                                    else{
                                        echo "{result:-200,text:create project must upload project media}";
                                            return;
                                    }  
                                }
                                else{
                                    echo "{result:-200,text:create project must upload project media}";
                                    return ;
                                }

                                $update = "UPDATE project_table SET project_name = :name, project_category =:category , project_ext_cost=:escost, project_duration=:duration, project_current_schedule=:schedule, project_paten_state=:haspaten, project_prototype_state=:hasprototype, inventor_country=:country, project_description=:description, project_media_url = :project_media_url WHERE project_id=:project_id";
                                $stmt = $file_db->prepare($update);

                                if($stmt == false){
                                    echo "\nPDO::errorInfo():\n";
                                    print_r($file_db->errorInfo());
                                    return;
                                }
                                // Bind parameters to statement variables
                                $stmt->bindParam(':name', $objPostParm->{'name'});
                                $stmt->bindParam(':category', $objPostParm->{'category'});
                                $stmt->bindParam(':escost', $objPostParm->{'escost'});
                                $stmt->bindParam(':duration', $objPostParm->{'duration'});
                                $stmt->bindParam(':schedule', $objPostParm->{'schedule'});
                                $stmt->bindParam(':haspaten', $objPostParm->{'haspaten'});
                                $stmt->bindParam(':hasprototype', $objPostParm->{'hasprototype'});
                                $stmt->bindParam(':country', $objPostParm->{'country'});
                                $stmt->bindParam(':description', $objPostParm->{'description'});
                                $stmt->bindParam(':project_media_url', $projectMedia_name);
                                $stmt->bindParam(':project_id', $objPostParm->{'project_id'});

                                // Execute statement
                                if($stmt->execute() > 0)
                                {
                                    $post_data = array(
                                        'result' => 0,
                                        'text' => "create project  success",
                                          'project_info' => array(
                                            'project_id' => $objPostParm->{'project_id'},
                                            'name' => $objPostParm->{'name'},
                                            'category' => $objPostParm->{'category'},
                                            'escost' => $objPostParm->{'escost'},
                                            'duration' => $objPostParm->{'duration'},
                                            'schedule' => $objPostParm->{'schedule'},
                                            'haspaten' => $objPostParm->{'haspaten'},
                                            'hasprototype' => $objPostParm->{'hasprototype'},
                                            'country' => $objPostParm->{'country'},
                                            'description' => $objPostParm->{'description'},
                                            'roleID' => $objPostParm->{'roleID'},
                                            'project_media_url' => $projectMedia_name
                                          )
                                    );

                                    echo json_encode($post_data);
                                        // echo "{result:0,text:registration success}";
                                }
                                else{
                                    // print_r($stmt->errorInfo());
                                    // echo "exec failed";
                                    echo "{result:-300,text:". $stmt->errorInfo() ."}";
                                }
                        }
                }
                else
                {
                        echo "{result:-101,text:HTTP POST parameter Error}";
                }
    }
    catch(PDOException $e)
    {
        echo "{result:-999,text:".var_dump($e->getMessage())."}";
        //echo 'Error:'. $e->getMessage();
    }

function check_parameter($jsonObject){

    if(!array_key_exists('name', $jsonObject)){
            return "{result:-10,parmeter error  missing 'project name'}";
    }
    else if(strlen($jsonObject->{'name'}) == 0){
            return "{result:-11,parmeter error 'project name' cannot empty}";
    }

    if(!array_key_exists('category', $jsonObject)){
                return "{result:-20,parmeter error  missing 'category'}";
        }
    else if(strlen($jsonObject->{'category'}) == 0){
        return "{result:-21,parmeter error 'category' cannot empty}";
    }
    else{
        $category_array = array("Muddy water" => 1, "Rain / River* Water" => 2, "Sea water" => 3, "Well water" => 4, "Sewage / Urine" => 5);

        if(!array_key_exists($jsonObject->{'category'}, $category_array)){
                return "{result:-22,parmeter error 'category' must be one of Muddy water, Rain / River* Water, Sea water, Well water or Sewage / Urine";
        }
    }

    if(!array_key_exists('escost', $jsonObject)){
                return "{result:-30,parmeter error  missing 'escost'}";
    }
    else if(strlen($jsonObject->{'escost'}) == 0){
            return "{result:-31,parmeter error 'escost' cannot empty}";
    }

    if(!array_key_exists('duration', $jsonObject)){
            return "{result:-40,parmeter error  missing 'duration'}";
    }
    else if(strlen($jsonObject->{'duration'}) == 0){
        return "{result:-41,parmeter error 'duration' cannot empty}";
    }

    if(!array_key_exists('schedule', $jsonObject)){
        return "{result:-50,parmeter error  missing 'schedule'}";
    }
    else if(strlen($jsonObject->{'schedule'}) == 0){
        return "{result:-51,parmeter error 'schedule' cannot empty}";
    }

    if(!array_key_exists('haspaten', $jsonObject)){
        return "{result:-60,parmeter error  missing 'haspaten'}";
    }
    else if(strlen($jsonObject->{'haspaten'}) == 0){
        return "{result:-61,parmeter error 'haspaten' cannot empty}";
    }

    if(!array_key_exists('hasprototype', $jsonObject)){
        return "{result:-70,parmeter error  missing 'hasprototype'}";
    }
    else if(strlen($jsonObject->{'hasprototype'}) == 0){
        return "{result:-71,parmeter error 'hasprototype' cannot empty}";
    }

    if(!array_key_exists('roleID', $jsonObject)){
        return "{result:-80,parmeter error  missing 'roleID'}";
    }
    else if(strlen($jsonObject->{'roleID'}) == 0){
        return "{result:-81,parmeter error 'roleID' cannot empty}";
    }

    return "";
    }
