<?php 
    ini_set('display_errors',1);
# echo "test";
// $userInfo = posix_getpwuid(posix_getuid());
// $user = $userInfo['name'];
// echo $user;

// $groupInfo = posix_getgrgid(posix_getgid());
// $group = $groupInfo = $groupInfo['name'];
// echo $group;
// echo 'Current script owner: ' . get_current_user();

$file_db = NULL;
$postParm = $_POST['parm'];
$personalImage_name = "";
$IDImageName = "";
$personalPath = "/purewater/personalImage/";
$IDPath = "/purewater/IDImage/";
$uploaddir = '/var/www/html';
 // [personalImage] => Array
 //        (
 //            [name] => waterImage.jpg
 //            [type] => image/jpeg
 //            [tmp_name] => /tmp/php0sdHU8
 //            [error] => 0
 //            [size] => 136157
 //        )

 //    [IDImage] => Array
 //        (
 //            [name] => 擷取.JPG
 //            [type] => image/jpeg
 //            [tmp_name] => /tmp/phpwUcY1D
 //            [error] => 0
 //            [size] => 90813
 //        )


// /
        //echo $postParm;
        //create sqlite db if not existed

        //parm={name:xxxx, password:xxxxx, DoB:xxx, email:xxx, address:xxx, telephone:xxx, role:xxx(inventor,investor,donator,charity) website:xx(optional), }

    try{

        $file_db = new PDO('sqlite:purewater.sqlite');
//echo "step1";
               	//parm={name:xxxx, password:xxxxx}
                if($postParm)
                {

                        $objPostParm = json_decode($postParm);
            $checkResult = check_parameter($objPostParm);

                        if(strlen($checkResult) > 0){
                                echo $checkResult;
                                return;
                        }

                         //check account exist
                        $sth=$file_db->prepare("select count(*) from role_table where email='" .$objPostParm->{'email'} ."'" . " AND role='" .$objPostParm->{'role'} ."'" );

                        $sth->execute();
                        $rowCount=$sth->fetchColumn(); //取得欄位1 的值  (也就是count(*))

                        //echo $postParm;
                        if(intval($rowCount) > 0)
                        {
                                echo "{\"result\":-100,\"text\":\"account existed\"}";
                        }
                        else
                        {
                                if (count($_FILES) > 0) {
                                    if(isset($_FILES["personalImage"])){
                                        if($_FILES["personalImage"]["error"] > 0){
                                            echo "{\"result\":-200,\"text\":\"image updload failed\"}";
                                            return;
                                        }
                                        // print_r($_FILES["personalImage"]);
                                        $personalImage_name = uniqid() . "_personal.jpg";

                                        $uploadfile = $uploaddir  . $personalPath . $personalImage_name;

                                        if (move_uploaded_file($_FILES['personalImage']['tmp_name'], $uploadfile)) {
                                            $personalImage_name = $personalPath . $personalImage_name;
                                        } else {
                                            echo "{\"result\":-201,\"text\":\"personal image updload failed\"}";
                                            return;
                                        }

                                    }
                                    if(isset($_FILES["IDImage"])){
                                        if($_FILES["IDImage"]["error"] > 0){
                                            echo "{\"result\":-200,\"text\":\"image updload failed\"}";
                                            return;
                                        }
                                        // print_r($_FILES["IDImage"]);   
                                        $IDImageName = uniqid() . "_ID.jpg";

                                        $uploadfile = $uploaddir . $IDPath . $IDImageName;

                                        if (move_uploaded_file($_FILES['IDImage']['tmp_name'], $uploadfile)) {
                                            $IDImageName = $IDPath . $IDImageName;
                                        } else {
                                            // echo "Possible file upload attack!\n";
                                            echo "{\"result\":-202,\"text\":\"ID image updload failed\"}";
                                        }
                                    }
                                }

                                $insert = "INSERT INTO role_table (name, DoB, email, address, password, telephone, website, role, role_Image_url, ID_Image_url, account_hash) VALUES (:name, :DoB, :email, :address, :password, :telephone, :website, :role, :role_Image_url, :ID_Image_url, :account_hash)";
                                $stmt = $file_db->prepare($insert);
                                $account_hash = uniqid();
                                // Bind parameters to statement variables
                                $stmt->bindParam(':name', $objPostParm->{'name'});
                                $stmt->bindParam(':DoB', $objPostParm->{'DoB'});
                                $stmt->bindParam(':email', $objPostParm->{'email'});
                                $stmt->bindParam(':address', $objPostParm->{'address'});
                                $stmt->bindParam(':password', $objPostParm->{'password'});
                                $stmt->bindParam(':telephone', $objPostParm->{'telephone'});
                                $stmt->bindParam(':website', $objPostParm->{'website'});
                                $stmt->bindParam(':role', $objPostParm->{'role'});
                                $stmt->bindParam(':role_Image_url', $personalImage_name);
                                $stmt->bindParam(':ID_Image_url', $IDImageName);
                                $stmt->bindParam(':account_hash', $account_hash);
                                // Execute statement
                                if($stmt->execute() > 0)
                                {
                                    $post_data = array(
                                        'result' => 0,
                                        'text' => "registration success",
                                          'role_info' => array(
                                            'role_id' => $file_db->lastInsertId(),
                                            'name' => $objPostParm->{'name'},
                                            'DoB' => $objPostParm->{'DoB'},
                                            'telephone' => $objPostParm->{'telephone'},
                                            'website' => $objPostParm->{'website'},
                                            'role' => $objPostParm->{'role'},
                                            'ID_Image_url' => $IDImageName,
                                            'role_Image_url' => $personalImage_name,
                                            'account_hash' => $account_hash
                                          )
                                    );

                                    echo json_encode($post_data);
                                        // echo "{result:0,text:registration success}";
                                }
                                else{
                                    // print_r($stmt->errorInfo());
                                    // echo "exec failed";
                                    echo "{\"result\":-300,\"text\":\"". $stmt->errorInfo() ."\"}";
                                }
                        }

                }
                else
                {
                        echo "{\"\"result\":-101,\"text\":\"HTTP POST parameter Error\"}";
                }
    }
    catch(PDOException $e)
    {
        echo "exception";
                echo "{\"result\":-999,\"text\":\"".$e->getMessage()."\"}";
        //echo 'Error:'. $e->getMessage();
    }

function check_parameter($jsonObject){

        if(!array_key_exists('name', $jsonObject)){
echo $jsonObject;
                return "{\"result\":-10,\"text\":\"parmeter error  missing 'name'\"}";
        }
        else if(strlen($jsonObject->{'name'}) == 0){
                return "{\"result\":-11,\"text\":\"parmeter error 'name' cannot empty\"}";
        }

    if(!array_key_exists('password', $jsonObject)){
                return "{\"result\":-20,\"text\":\"parmeter error  missing 'password'\"}";
        }
    else if(strlen($jsonObject->{'password'}) == 0){
                return "{\"result\":-21,\"text\":\"parmeter error 'password' cannot empty\"}";
        }

    if(!array_key_exists('DoB', $jsonObject)){
                return "{\"result\":-30,\"text\":\"parmeter error  missing 'DoB'\"}";
        }
    else if(strlen($jsonObject->{'DoB'}) == 0){
                return "{\"result\":-31,\"text\":\"parmeter error 'DoB' cannot empty\"}";
        }

    if(!array_key_exists('email', $jsonObject)){
                    return "{\"result\":-40,\"text\":\"parmeter error  missing 'email'\"}";
            }
        else if(strlen($jsonObject->{'email'}) == 0){
                            return "{\"result\":-41,\"text\":\"parmeter error 'email' cannot empty\"}";
            }

        if(!array_key_exists('address', $jsonObject)){
                    return "{\"result\":-50,\"text\":\"parmeter error  missing 'address'\"}";
            }
        else if(strlen($jsonObject->{'address'}) == 0){
                            return "{\"result\":-51,\"text\":\"parmeter error 'address' cannot empty\"}";
            }

        if(!array_key_exists('telephone', $jsonObject)){
                    return "{\"result\":-60,\"text\":\"parmeter error  missing 'telephone'\"}";
            }
        else if(strlen($jsonObject->{'telephone'}) == 0){
                            return "{\"result\":-61,\"text\":\"parmeter error 'telephone' cannot empty\"}";
            }

        if(!array_key_exists('role', $jsonObject)){
                return "{\"result\":-70,\"text\":\"parmeter error  missing 'role'\"}";
        }
    else{
                if(strlen($jsonObject->{'role'}) == 0){
                        return "{\"result\":-71,\"text\":\"parmeter error 'role' cannot empty\"}";
                }
                else{
                        $role_array = array("inventor" => 1, "investor" => 2, "donator" => 3, "charity" => 4);

                        if(!array_key_exists($jsonObject->{'role'}, $role_array)){
                                return "{\"result\":-72,\"text\":\"parmeter error 'role' must be one of inventor, investor, donator or charity\"}";
                        }
                }
        }

    return "";
    }
