<?php
    error_reporting(E_ALL);
    ini_set('display_errors',1);
# echo "test";
// $userInfo = posix_getpwuid(posix_getuid());
// $user = $userInfo['name'];
// echo $user;

// $groupInfo = posix_getgrgid(posix_getgid());
// $group = $groupInfo = $groupInfo['name'];
// echo $group;
// echo 'Current script owner: ' . get_current_user();

$file_db = NULL;
$postParm = $_POST['parm'];

 

    try{

        $file_db = new PDO('sqlite:purewater.sqlite');
//echo "step1";
               	//parm={name:xxxx, password:xxxxx}
                if($postParm)
                {

                    $objPostParm = json_decode($postParm);
                    $checkResult = check_parameter($objPostParm);

                    if(strlen($checkResult) > 0){
                        echo $checkResult;
                        return;
                    }

                         //check account exist
                        $sth=$file_db->prepare("select count(*) from project_table where project_id=".$objPostParm->{'project_id'} );

                        $sth->execute();
                        $rowCount=$sth->fetchColumn(); //取得欄位1 的值  (也就是count(*))

                        //echo $postParm;
                        if(intval($rowCount) == 0)
                        {
                                echo "{result:-100,text:project doesn't existed}";
                                return;
                        }

                         //check account exist
                        $sth=$file_db->prepare("select count(*) from role_table where role_id=".$objPostParm->{'role_id'} . " AND role = 'charity'");

                        $sth->execute();
                        $rowCount=$sth->fetchColumn(); //取得欄位1 的值  (也就是count(*))
                        //echo $postParm;
                        if(intval($rowCount) == 0)
                        {
                                echo "{result:-101,text:charity doesn't existed}";
                                return;
                        }

                         //check account exist
                        $sth=$file_db->prepare("select count(*) from charity_project_relation where charity_id=" .$objPostParm->{'role_id'} . " AND project_id =" . $objPostParm->{'project_id'} );

                        $sth->execute();
                        $rowCount=$sth->fetchColumn(); //取得欄位1 的值  (也就是count(*))
                        //echo $postParm;
                        if(intval($rowCount) > 0)
                        {
                                echo "{result:-102,text:the project already under a charity}";
                                return;
                        }
                        else
                        {
                                $insert = "INSERT INTO charity_project_relation(project_id, charity_id) 
                                            VALUES (:project_id, :charity_id)";

                                $stmt = $file_db->prepare($insert);
                                if($stmt == false){
                                    echo "\nPDO::errorInfo():\n";
                                    print_r($file_db->errorInfo());
                                    return;
                                }
                                // Bind parameters to statement variables
                                $stmt->bindParam(':project_id', $objPostParm->{'project_id'});
                                $stmt->bindParam(':charity_id', $objPostParm->{'role_id'});
                                

                                // Execute statement
                                if($stmt->execute() > 0)
                                {
                                    $post_data = array(
                                        'result' => 0,
                                        'text' => "add project to charity  success",
                                          'donate_info' => array(
                                            'project_id' => $objPostParm->{'project_id'},
                                            'role_id' => $objPostParm->{'role_id'}
                                          )
                                    );

                                    echo json_encode($post_data);
                                        // echo "{result:0,text:registration success}";
                                }
                                else{
                                    // print_r($stmt->errorInfo());
                                    // echo "exec failed";
                                    echo "{result:-300,text:". $stmt->errorInfo() ."}";
                                }
                        }
                }
                else
                {
                        echo "{result:-103,text:HTTP POST parameter Error}";
                }
    }
    catch(PDOException $e)
    {
        echo "{result:-999,text:".var_dump($e->getMessage())."}";
        //echo 'Error:'. $e->getMessage();
    }

function check_parameter($jsonObject){

    if(!array_key_exists('project_id', $jsonObject)){
            return "{result:-10,parmeter error  missing 'project_id'}";
    }
    else if(strlen($jsonObject->{'project_id'}) == 0){
            return "{result:-11,parmeter error 'project_id' cannot empty}";
    }

    if(!array_key_exists('role_id', $jsonObject)){
                return "{result:-20,parmeter error  missing 'role_id'}";
        }
    else if(strlen($jsonObject->{'role_id'}) == 0){
        return "{result:-21,parmeter error 'role_id' cannot empty}";
    }

    return "";
    }
