package com.hack.purewater.android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.login.LoginFragment;
import com.hack.purewater.android.fragment.InventorFragment;
import com.hack.purewater.android.util.Constants;
import com.hack.purewater.android.util.LOG;
import com.hack.purewater.android.util.WaterUtils;
import com.hack.purewater.android.widget.WaitingProgressView;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import static android.view.View.GONE;
import static com.hack.purewater.android.util.Constants.FRAGMENT_TAG_INVENTOR_FRAGMENT;

/**
 * Created by Giles on 2017/12/24.
 */

public class WaterMainActivity extends AppCompatActivity {
    private final static String TAG = "WaterMainActivity";

    public static final String KEY_IS_FROM_LANDING_PAGE = "key-is-from-landing-page";

    private static final int DELAY_TIME = 1000;

    private Context mContext;
    private SharedPreferences mSharedPreference = null;

    private RelativeLayout mActionBar = null;
    private RelativeLayout mActionLayoutLogout = null;
    private TextView mActionBarTitle = null;
    private ImageView mActionBarBack = null;

    private WaitingProgressView mProgress = null;

    private InventorFragment mInventorFragment = null;

//    private boolean mIsFirstInstallLaunch = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_water_main);
        mContext = this;
        mSharedPreference = getSharedPreferences(Constants.PREF_NAME_PURE_WATER, Context.MODE_PRIVATE);

        initView();
        initActionBar();
        initImageLoader();

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
//            mIsFromLandingPage = bundle.getBoolean(KEY_IS_FROM_LANDING_PAGE, false);
            bundle.clear();
        }



    }

    @Override
    public void onStart() {
        super.onStart();
        LOG.D(TAG,"onStart");

    }

    @Override
    protected void onNewIntent(Intent newIntent) {
        super.onNewIntent(newIntent);
        LOG.V(TAG, "onNewIntent() - start");

    }

    @Override
    protected void onResume() {
        super.onResume();

        LOG.V(TAG, "onResume()" );

        if(mInventorFragment == null){
            launchFragment(FRAGMENT_TAG_INVENTOR_FRAGMENT, false);
        }




    }

    @Override
    public void onBackPressed() {
        LOG.D(TAG, "onBackPressed");
        LOG.D(TAG, "mProgress.isActivated() = " + mProgress.isActivated());
        LOG.D(TAG, "mProgress.getVisibility() = " + mProgress.getVisibility());

        if(mProgress.getVisibility() == View.VISIBLE){
            mProgress.setVisiableWithAnimate(GONE);
        }else{
            super.onBackPressed();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        // TODO Auto-generated method stub
        LOG.V(TAG, "onPause() - start");
//        unregisterLocationListener();
        LOG.V(TAG, "onPause() - end");
    }

    @Override
    protected void onStop() {
        super.onStop();

        LOG.V(TAG, "onStop() - start ");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LOG.V(TAG, "onDestroy() - start");
    }


    private void initView(){
        mProgress = (WaitingProgressView) findViewById(R.id.progress);
    }

    private void initActionBar(){
        mActionBar = (RelativeLayout) findViewById(R.id.top_action_bar);

        mActionBarTitle = (TextView) mActionBar.findViewById(R.id.txt_action_bar_title);
//        mActionLayoutLogout = (RelativeLayout) mActionBar.findViewById(R.id.action_layout_logout);
//        mActionBarBack = (ImageView) mActionBar.findViewById(R.id.img_back);
//        mActionBarBack.setTag(R.mipmap.icon_back);
//
//        mActionBarBack.setOnClickListener(mActionBarBackClickListener);
//
//        mActionLayoutLogout.setOnClickListener(mActionLayoutLogoutClickListener);

    }

    private void initImageLoader(){
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
//            .diskCache(new UnlimitedDiscCache(cacheDir)) // default
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(20 * 1024 * 1024))
                .diskCacheFileCount(100)
                .defaultDisplayImageOptions(WaterUtils.getCustomDisplayImageOptions(R.mipmap.ic_launcher))
//            .defaultDisplayImageOptions(options)
//                .diskCacheExtraOptions(480, 320, null)
                .threadPoolSize(10)
                .build();

        ImageLoader.getInstance().init(config);
    }

    private void launchFragment(int tag, boolean removeChooseList) {
        LOG.V(TAG, "[launchFragment] tag = " + tag);


        FragmentManager fm = getSupportFragmentManager();
        //FragmentManager fm = getChildFragmentManager();
        if(fm == null) {
            LOG.W(TAG, "[launchFragment] FragmentManager is null.");
            return;
        }

        FragmentTransaction ft = fm.beginTransaction();
        if(ft == null) {
            LOG.W(TAG, "[launchFragment] FragmentTransaction is null.");
            return;
        }

        Bundle arguments= new Bundle();

        switch (tag) {

            case FRAGMENT_TAG_INVENTOR_FRAGMENT :
                mInventorFragment = new InventorFragment();
                ft.replace(Constants.WATER_CONTENT_FRAME_ID, mInventorFragment, InventorFragment.TAG).commitAllowingStateLoss();
                break;

        }

    }

    /***********************************************  MainActivity function******************************************************/
    public void setProgressShow(boolean isShowProgress){
        if(isShowProgress){
            mProgress.setVisiableWithAnimate(View.VISIBLE);
        }else{
            mProgress.setVisiableWithAnimate(GONE);
        }
    }


    /***********************************************  action bar function******************************************************/
    public void setActionBarLogoutShow(boolean isShow)
    {
        if(isShow){
            mActionLayoutLogout.setVisibility(View.VISIBLE);
        }else{
            mActionLayoutLogout.setVisibility(GONE);
        }
    }

    public void setActionBarTitle(String title)
    {
        LOG.D(TAG,"setActionBarTitle title = " + title);
        mActionBarTitle.setText(title);
    }

    public void setActionBarShow(boolean isShow){
        if(isShow){
            mActionBar.setVisibility(View.VISIBLE);
        }else{
            mActionBar.setVisibility(GONE);
        }
    }

    public void setActionBarBackShow(boolean isShow){
        if(isShow){
            mActionBarBack.setVisibility(View.VISIBLE);
        }else{
            mActionBarBack.setVisibility(GONE);
        }
    }

    public void setActionBarBackResource(int resId){
        mActionBarBack.setImageResource(resId);
        mActionBarBack.setTag(resId);
    }

//    public void setErrorDialogShow(int errorCode, String errorContent){
//        LOG.D(TAG,"mErrorDialog = " + mErrorDialog);
//        if(mErrorDialog != null){
//            LOG.D(TAG,"mErrorDialog isShowing = " + mErrorDialog.isShowing());
//        }
//        mErrorDialog = new ErrorDialog(mContext, WaterMainActivity.this, errorCode, errorContent);
//        mErrorDialog.show();
//    }


}
