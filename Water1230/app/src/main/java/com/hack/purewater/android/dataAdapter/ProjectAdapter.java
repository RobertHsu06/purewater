package com.hack.purewater.android.dataAdapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hack.purewater.android.R;
import com.hack.purewater.android.WaterMainActivity;
import com.hack.purewater.android.util.LOG;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;


/**
 * Created by Giles on 2017/12/25.
 */

public abstract class ProjectAdapter extends BaseAdapter {

    private static final String TAG = "ProjectAdapter";
    private LayoutInflater mInflater = null;
    private WaterMainActivity mWaterMainActivity;

    private int mRoomType = -1;
    private String mStartDate = null;
    private String mEndDate = null;

    public abstract void getData();

    public ProjectAdapter(WaterMainActivity activity) {
        mWaterMainActivity = activity;
        mInflater = (LayoutInflater) mWaterMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    public void setDate(){

    }



    @Override
    public int getCount() {
//        if (getData() != null) {
//            return getData().size();
//        }
        return 4;

//        return 20;
    }

    @Override
    public Object getItem(int position) {
//        if (getData() != null) {
//            return getData().get(position);
//        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item_project, parent, false);
        }

        LOG.D(TAG,"getView position = " + position);
//        final HotelListDataItemsModel hotelListDataItems = (HotelListDataItemsModel) getItem(position);


//        ImageView imgHotel = (ImageView) convertView.findViewById(R.id.img_hotel);
//
//
//        txtRatingDesc.setText(hotelListDataItems.getReview());
//
//
//        btnPrice.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                LOG.D(TAG,"btnPrice onClick");
//                launchHotelWeb(position);
//
//
//            }
//        });

        return convertView;
    }
}
