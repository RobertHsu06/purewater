package com.hack.purewater.android.util.apitask;

import android.content.Context;
import android.os.AsyncTask;

import com.hack.purewater.android.dataModel.UserInfoModel;
import com.hack.purewater.android.dataModel.WaterRegisterModel;
import com.hack.purewater.android.util.ApiAccessor;
import com.hack.purewater.android.util.ApiResultParser;
import com.hack.purewater.android.util.LOG;
import com.hack.purewater.android.util.apitask.listener.IUserInfoListener;
import com.hack.purewater.android.util.apitask.listener.IWaterRegisterListener;

/**
 * Created by Giles on 2017/12/25.
 */

public class WaterRegisterTask extends AsyncTask<Object, Integer, WaterRegisterModel> {
    public static final String TAG = "WaterRegisterTask";

    IWaterRegisterListener callback;

    @Override
    protected WaterRegisterModel doInBackground(Object... params) {
        Context context = (Context) params[0];
        String name = (String) params[1];
        String password = (String) params[2];
        String DoB = (String) params[3];
        String email = (String) params[4];
        String address = (String) params[5];
        String telephone = (String) params[6];
        String role = (String) params[7];
        String website = (String) params[8];
        callback = (IWaterRegisterListener) params[9];

        WaterRegisterModel model = null;

        try {

//            model = ApiResultParser.loginDataParser(ApiAccessor.login(context, phoneNumber));
            model = ApiResultParser.waterRegisterParser(ApiAccessor.waterRegister(context, name, password, DoB, email, address, telephone, role, website));

        } catch (Throwable tr) {
            LOG.E(TAG, "WaterRegisterModel() - failed.", tr);
        }

        return model;
    }

    @Override
    protected void onPostExecute(WaterRegisterModel result) {

        if(callback != null)
            callback.onWaterRegister(result);
    }
}
