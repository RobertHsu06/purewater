package com.hack.purewater.android.util.apitask.listener;

import com.hack.purewater.android.dataModel.UserInfoModel;

/**
 * Created by ggshao on 2017/11/30.
 */

public interface IUserInfoListener {
    public void onUserInfo(UserInfoModel model);
}
