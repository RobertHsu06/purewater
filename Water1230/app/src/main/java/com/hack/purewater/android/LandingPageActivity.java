package com.hack.purewater.android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.hack.purewater.android.fragment.LoginWaterFragment;
import com.hack.purewater.android.util.Constants;
import com.hack.purewater.android.util.LOG;

/**
 * Created by ggshao on 2017/9/7.
 */

public class LandingPageActivity extends AppCompatActivity {

    private static final String TAG = "LandingPageActivity";
    private static final int DELAY_TIME = 1000;

    private SharedPreferences mSharedPreference;

    private Button mBtnGoToMain;
    public LandingPageActivity mCurrentActivity;
    Context mContext = null;

    private ImageView mImgSplash = null;

    private LoginWaterFragment mLoginWaterFragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_page);


        mContext = this;
        initView();
        mSharedPreference = this.getSharedPreferences(Constants.PREF_NAME_PURE_WATER, Context.MODE_PRIVATE);
        mCurrentActivity = LandingPageActivity.this;
        // Disable landing page
        boolean mIsLogin = mSharedPreference.getBoolean(Constants.PREF_KEY_IS_LOGIN, false);

        //test GGGG data
        mIsLogin = false;
//        mIsFirstInstallLaunch = false;


        if(mIsLogin){
            new CountDownTimer(DELAY_TIME, 100) {

                @Override
                public void onTick(long millisUntilFinished) {
                }

                @Override
                public void onFinish() {
                    mImgSplash.setVisibility(View.GONE);
                    goToWaterMainActivity();
                    //goto main
                }
            }.start();
        }else{
            //go to login fragment
            mImgSplash.setVisibility(View.GONE);
            launchFragment(Constants.FRAGMENT_TAG_WATER_LOGIN);


        }


    }

    private void launchFragment(int tag) {
        LOG.V(TAG, "[launchFragment] tag = " + tag );


        FragmentManager fm = getSupportFragmentManager();

        if(fm == null) {
            LOG.W(TAG, "[launchFragment] FragmentManager is null.");
            return;
        }

        // Clear back stack and keep base fragment when change main category.
        LOG.V(TAG, "launchFragment fm.getBackStackEntryCount() = " + fm.getBackStackEntryCount());
//        if(fm.getBackStackEntryCount() > 0) {
//            fm.popBackStack(0, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//        }

        FragmentTransaction ft = fm.beginTransaction();
        if(ft == null) {
            LOG.W(TAG, "[launchFragment] FragmentTransaction is null.");
            return;
        }

        Bundle arguments = new Bundle();

        switch (tag) {

            case Constants.FRAGMENT_TAG_WATER_LOGIN:
                mLoginWaterFragment = new LoginWaterFragment();

//                arguments.putString(WebViewFragment.KEY_WEB_VIEW_URL, webViewUrl);
//                mHomeFragment.setArguments(arguments);

                ft.replace(Constants.LANDING_FRAME_ID, mLoginWaterFragment, LoginWaterFragment.TAG).commitAllowingStateLoss();
                break;

        }

    }

    public void goToWaterMainActivity() {


        mSharedPreference.edit().putBoolean(Constants.PREF_KEY_IS_LOGIN, true).apply();
        Intent intent = new Intent(this, WaterMainActivity.class);
//        intent.putExtra(MainActivity.KEY_IS_FROM_LANDING_PAGE, isFromLadingPage);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // if(mViewPager != null) {
        // mViewPager.setAdapter(null);
        // mViewPager = null;
        // }
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }


    private void initView(){
        mImgSplash = (ImageView) findViewById(R.id.mImgSplash);
    }
}
