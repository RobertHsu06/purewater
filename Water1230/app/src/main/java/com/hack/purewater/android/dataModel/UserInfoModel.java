package com.hack.purewater.android.dataModel;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by ggshao on 2017/11/30.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInfoModel {
    private int code = -1;
//    private UserInfoDataModel userInfoData = null;
    //
    @JsonProperty("code")
    public int getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(int code) {
        this.code = code;
    }
    //
//    @JsonProperty("data")
//    public UserInfoDataModel getUserInfoData() {
//        return userInfoData;
//    }
//    @JsonProperty("data")
//    public void setUserInfoData(UserInfoDataModel userInfoData) {
//        this.userInfoData = userInfoData;
//    }
}
