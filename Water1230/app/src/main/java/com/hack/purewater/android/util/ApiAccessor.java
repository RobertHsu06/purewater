package com.hack.purewater.android.util;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ggshao on 2017/7/17.
 */

public class ApiAccessor {
    private static final String TAG = "ApiAccessor";
//    public static String SERVER_IP = "https://taiwantraffictest.ddns.net:3001";
    public static String SERVER_IP = "http://taiwantrafficmqtt.ddns.net:3001";
    public static String WATER_SERVER_IP = "http://123.193.224.62:8099";
    public static String TRIVAGO_SERVER_IP = "https://www.trivago.com.tw";




    public static URI GAME_WEBSOCKET_URI = null;
    public static URI AUDIO_WEBSOCKET_URI = null;
//    public static URI GAME_WEBSOCKET_URI = null;


    //https://taiwantraffic.ddns.net:3001    pro
    //https://taiwantraffictest.ddns.net:3001    dev

    public static String GOOGLE_DIRECTION_IP = "https://maps.googleapis.com/maps";


    private final static String GET_DIRECTION_PATH = "/api/directions/json?";


//    static NSString* PRODUCT_API_URL = @"https://taiwantraffic.ddns.net:3001";
//    static NSString* DEV_API_URL = @"https://taiwantraffictest.ddns.net:3001";
//    static NSString* PRODUCT_SOCKET_URL = @"wss://taiwantraffic.ddns.net:5684/ws";
//    static NSString* DEV_SOCKET_URL = @"wss://taiwantraffictest.ddns.net:5684/ws";
//
//    static NSString* PRODUCT_CHAT_SOCKET_URL = @"wss://taiwantraffic.ddns.net:8684/ws";
//    static NSString* DEV_CHAT_SOCKET_URL = @"wss://taiwantraffictest.ddns.net:8684/ws";
//
//    static NSString* PRODUCT_AUDIO_SOCKET_URL = @"wss://taiwantraffic.ddns.net:3684/ws";
//    static NSString* DEV_AUDIO_SOCKET_URL = @"wss://taiwantraffictest.ddns.net:3684/ws";


    //    public static String LOCATION_SERVER_IP = "http://210.242.49.249";
    private final static String GET_ACCESS_TOKEN_PATH = "/api/v1/sign_in?";
    private final static String LOGIN_PATH = "/api/v1/login/login?";
    private final static String REQ_SMS_SIGN_UP_PHONE_PATH = "/api/v1/setting/req_sms_signup_Phone?";
    private final static String SIGN_UP_PHONE_PATH = "/api/v1/signup/signup_phone?";


    private final static String CREATE_ROOM_PATH = "/api/v1/game/create_room?";
    private final static String JOIN_ROOM_PATH = "/api/v1/game/join_room?";



    private final static String SIGN_UP_PATH = "/api/v1/account/signup";
    private final static String SIGN_UP_FB_PATH = "/api/v1/account/signup_fb";
    private final static String SIGN_IN_PATH = "/api/v1/account/signin";
    private final static String SIGN_IN_FB_PATH = "/api/v1/account/signin_fb";

    private final static String FORGET_PASSWORD_EMAIL_PATH = "/api/v1/account/forget_password_email";
    private final static String BIND_PHONE_SMS_PATH = "/api/v1/setting/bind_phone_sms";
    private final static String BIND_PHONE_PATH = "/api/v1/setting/bind_phone";
    private final static String FORGET_PASSWORD_PATH = "/api/v1/account/forget_password";
    private final static String FORGET_PASSWORD_CODE_PATH = "/api/v1/account/forget_password_code";
    private final static String RESET_PASSWORD_PATH = "/api/v1/account/reset_password";
    private final static String SETTING_PATH = "/api/v1/setting/setting";
    private final static String HOTEL_SUGGESTION_PATH = "/api/v1/hotel/suggestion";
    private final static String HOTEL_LIST_URL_PATH = "/api/v1/hotel/hotel_list_url";
    private final static String HOTEL_DETAIL_URL_PATH = "/api/v1/hotel/hotel_detail_url";
    private final static String HOTEL_LIST_PATH = "/api/v1/hotel/hotel_list";
    private final static String HOTEL_DETAIL_PATH = "/api/v1/hotel/hotel_detail";
    private final static String HOTEL_URL_PATH = "/api/v1/hotel/hotel_url";


    private final static String Y_POINT_PATH = "/api/v1/mart/ypoint";
    private final static String MART_PRODUCT_LIST_PATH = "/api/v1/mart/product_list";
    private final static String MART_PRODUCT_PATH = "/api/v1/mart/product";
    private final static String MART_PURCHASE_PRODUCT_PATH = "/api/v1/mart/purchase_product";


    private final static String POCKET_LIST_PATH = "/api/v1/pocket/pocket_list";
    private final static String UPLOAD_PAGE_PATH = "/api/v1/hotel/upload_page";
    private final static String REQUEST_ROAMING_PATH = "/api/v1/pocket/request_roaming";
    private final static String USER_INFO_PATH = "/api/v1/account/user_info";


    private final static String WATER_REGISTER_PATH = "/purewater/registration.php";



    private final static String TRIVAGO_SEARCH_HOTEL_PATH = "/search/tw-TW-TW/v11_06_2_ad_cache/suggest_concepts?";






    public static JSONObject login(Context context, String account, String password) throws JSONException {
        if(account == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("name", account));
        params.add(new BasicNameValuePair("pass", password));

        HttpHandler http = new HttpHandler(SERVER_IP + LOGIN_PATH, params);
        resultJson = http.getResponseByPost();
        LOG.D(TAG, " login resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }
        return new JSONObject(resultJson);

    }

    public static JSONObject reqSmsSignUpPhone(Context context, String phoneNumber) throws JSONException {
        if(phoneNumber == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("phone", phoneNumber));

        HttpHandler http = new HttpHandler(SERVER_IP + REQ_SMS_SIGN_UP_PHONE_PATH, params);
        resultJson = http.getResponseByPost();
        LOG.D(TAG, " reqSmsSignUpPhone resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }
        return new JSONObject(resultJson);

    }

    public static JSONObject signUpPhone(Context context, String account, String password, String phoneNumber, String smsCode) throws JSONException {
        if(phoneNumber == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("name", account));
        params.add(new BasicNameValuePair("pass", password));
        params.add(new BasicNameValuePair("phone", phoneNumber));
        params.add(new BasicNameValuePair("code", smsCode));

        HttpHandler http = new HttpHandler(SERVER_IP + SIGN_UP_PHONE_PATH, params);
        resultJson = http.getResponseByPost();
        LOG.D(TAG, " signUpPhone resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }
        return new JSONObject(resultJson);

    }

    public static JSONObject signUpFb(Context context, String accessToken, boolean allowPromote, String locale) throws JSONException {
        if(accessToken == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("accessToken", accessToken));
        params.add(new BasicNameValuePair("allowPromote", String.valueOf(allowPromote)));
        params.add(new BasicNameValuePair("locale", locale));

        HttpHandler http = new HttpHandler(SERVER_IP + SIGN_UP_FB_PATH, params);
        resultJson = http.getResponseByPost();
        LOG.D(TAG, " signUpFb resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }
        return new JSONObject(resultJson);

    }

    public static JSONObject createRoom(Context context, String loginToken, String mapGuid, String gameType, String reverse) throws JSONException {
        if(loginToken == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("token", loginToken));
        params.add(new BasicNameValuePair("mapguid", mapGuid));
        params.add(new BasicNameValuePair("gametype", gameType));
        params.add(new BasicNameValuePair("reverse", reverse));

        HttpHandler http = new HttpHandler(SERVER_IP + CREATE_ROOM_PATH, params);
        resultJson = http.getResponseByPost();
        LOG.D(TAG, " createRoom resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }
        return new JSONObject(resultJson);

    }

    public static JSONObject joinRoom(Context context, String loginToken, String roomId, String gameType) throws JSONException {
        if(loginToken == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("token", loginToken));
        params.add(new BasicNameValuePair("room", roomId));
        params.add(new BasicNameValuePair("gametype", gameType));

        HttpHandler http = new HttpHandler(SERVER_IP + JOIN_ROOM_PATH, params);
        resultJson = http.getResponseByPost();
        LOG.D(TAG, " joinRoom resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }
        return new JSONObject(resultJson);

    }




    public static JSONObject getAccessToken(Context context, String name) throws JSONException {
        if(name == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("u_name", name));

        HttpHandler http = new HttpHandler(SERVER_IP + GET_ACCESS_TOKEN_PATH, params);
        resultJson = http.getResponseByPost();
        LOG.D(TAG, " getAccessToken resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }
        return new JSONObject(resultJson);

    }

    public static JSONObject getRouteFromGoogle(Context context, LatLng origin, LatLng destination, List<LatLng> wayPoints) throws JSONException {
        if(origin == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("origin", origin.latitude + "," + origin.longitude));
        params.add(new BasicNameValuePair("destination", destination.latitude + "," + destination.longitude));

        String wayPointsString = "";
        if(wayPoints != null){
            for(int i = 0 ; i < wayPoints.size() ; i++){
                wayPointsString += wayPoints.get(i).latitude + "," + wayPoints.get(i).longitude;
            }

            params.add(new BasicNameValuePair("waypoints", wayPointsString));
        }

        LOG.D(TAG,"wayPointsString = " + wayPointsString);

        HttpHandler http = new HttpHandler(GOOGLE_DIRECTION_IP + GET_DIRECTION_PATH, params);
        resultJson = http.getResponseByGet();
        LOG.D(TAG, " getRouteFromGoogle resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }
        return new JSONObject(resultJson);

    }

    public static JSONObject signUp(Context context, String firstName, String lastName, String account, String password) throws JSONException {
        if(account == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

//        params.add(new BasicNameValuePair("firstName", firstName));
//        params.add(new BasicNameValuePair("lastName", lastName));
        params.add(new BasicNameValuePair("account", account));
        params.add(new BasicNameValuePair("password", password));


        HttpHandler http = new HttpHandler(SERVER_IP + SIGN_UP_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " signUp resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }
        return new JSONObject(resultJson);

    }

    public static JSONObject signIn(Context context, String account, String password, String deviceId, String deviceName, String os, String devToken ) throws JSONException {
        if(account == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("deviceId", deviceId));
        params.add(new BasicNameValuePair("deviceName", deviceName));
        params.add(new BasicNameValuePair("account", account));
        params.add(new BasicNameValuePair("password", password));
        params.add(new BasicNameValuePair("os", os));
        params.add(new BasicNameValuePair("devToken", devToken));


        HttpHandler http = new HttpHandler(SERVER_IP + SIGN_IN_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, "signIn resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }
        return new JSONObject(resultJson);

    }

    public static JSONObject signInFb(Context context, String accessToken, String deviceId, String deviceName, String deviceToken, String os) throws JSONException {
        if(accessToken == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("devId", deviceId));
        params.add(new BasicNameValuePair("devName", deviceName));
        params.add(new BasicNameValuePair("accessToken", accessToken));
        params.add(new BasicNameValuePair("devToken", deviceToken));
        params.add(new BasicNameValuePair("os", os));


        HttpHandler http = new HttpHandler(SERVER_IP + SIGN_IN_FB_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " signInFb resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }
        return new JSONObject(resultJson);

    }

    public static JSONObject forgetPasswordEmail(Context context, String email) throws JSONException {
        if(email == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("email", email));


        HttpHandler http = new HttpHandler(SERVER_IP + FORGET_PASSWORD_EMAIL_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " forgetPasswordEmail resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }
        return new JSONObject(resultJson);

    }

    public static JSONObject bindPhoneSms(Context context, String token, String phone, String countryCode) throws JSONException {
        if(phone == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("token", token));
        params.add(new BasicNameValuePair("phone", phone));
        params.add(new BasicNameValuePair("countryCode", countryCode));



        HttpHandler http = new HttpHandler(SERVER_IP + BIND_PHONE_SMS_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " bindPhoneSms resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }
        return new JSONObject(resultJson);

    }

    public static JSONObject bindPhone(Context context, String token, String phone, String code, String countryCode) throws JSONException {
        if(phone == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("token", token));
        params.add(new BasicNameValuePair("phone", phone));
        params.add(new BasicNameValuePair("code", code));
        params.add(new BasicNameValuePair("countryCode", countryCode));



        HttpHandler http = new HttpHandler(SERVER_IP + BIND_PHONE_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " bindPhone resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }

        return new JSONObject(resultJson);

    }

    public static JSONObject forgetPassword(Context context, String email, String phone) throws JSONException {

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        if(email != null){
            params.add(new BasicNameValuePair("email", email));
        }

        if(phone != null){
            params.add(new BasicNameValuePair("phone", phone));
        }

        HttpHandler http = new HttpHandler(SERVER_IP + FORGET_PASSWORD_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " forgetPassword resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }

        return new JSONObject(resultJson);

    }

    public static JSONObject forgetPasswordCode(Context context, String email, String phone, String code) throws JSONException {
        if(code == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        if(email != null){
            params.add(new BasicNameValuePair("email", email));
        }

        if(phone != null){
            params.add(new BasicNameValuePair("phone", phone));
        }

        params.add(new BasicNameValuePair("code", code));

        HttpHandler http = new HttpHandler(SERVER_IP + FORGET_PASSWORD_CODE_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " forgetPasswordCode resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }

        return new JSONObject(resultJson);

    }

    public static JSONObject resetPassword(Context context, String key, String password) throws JSONException {
        if(password == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("key", key));
        params.add(new BasicNameValuePair("password", password));

        HttpHandler http = new HttpHandler(SERVER_IP + RESET_PASSWORD_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " resetPassword resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }

        return new JSONObject(resultJson);

    }


    public static JSONObject setting(Context context, String token, boolean notification, String locale, String distanceUnit,
                                     String currency) throws JSONException {
        if(token == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("token", token));
        params.add(new BasicNameValuePair("notification", String.valueOf(notification)));
        params.add(new BasicNameValuePair("locale", locale));
        params.add(new BasicNameValuePair("distanceUnit", distanceUnit));
        params.add(new BasicNameValuePair("currency", currency));

        HttpHandler http = new HttpHandler(SERVER_IP + SETTING_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " setting resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }

        return new JSONObject(resultJson);

    }


    public static JSONObject trivagoSearchHotel(Context context, String q) throws JSONException {
        if(q == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("q", q));

        HttpHandler http = new HttpHandler(TRIVAGO_SERVER_IP + TRIVAGO_SEARCH_HOTEL_PATH, params);
        resultJson = http.getResponseByGet();
        LOG.D(TAG, " trivagoSearchHotel resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }

        return new JSONObject(resultJson);

    }

    public static JSONObject hotelSuggestion(Context context, String token, String q) throws JSONException {
        if(q == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("token", token));
        params.add(new BasicNameValuePair("q", q));

        LOG.D(TAG,"q = " + q);

        HttpHandler http = new HttpHandler(SERVER_IP + HOTEL_SUGGESTION_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " hotelSuggestion resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }

        return new JSONObject(resultJson);

    }


    public static JSONObject hotelListUrl(Context context, String token, int cpt, int iPathId,
                                          String startDate, String endDate, int adult) throws JSONException {
        if(token == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("token", token));
        params.add(new BasicNameValuePair("cpt", String.valueOf(cpt)));
        params.add(new BasicNameValuePair("iPathId", String.valueOf(iPathId)));
        params.add(new BasicNameValuePair("startdate", startDate));
        params.add(new BasicNameValuePair("enddate", endDate));
        params.add(new BasicNameValuePair("adult", String.valueOf(adult)));

        HttpHandler http = new HttpHandler(SERVER_IP + HOTEL_LIST_URL_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " hotelListUrl resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }

        return new JSONObject(resultJson);

    }

    public static JSONObject hotelListUrl(Context context, String token, int cpt, int iPathId,
                                          String startDate, String endDate, int adult, int offset, int limit, int viewType, double lat,
                                          double lng, double latBottom, double lngBottom, double latTop, double lngTop) throws JSONException {
        if(token == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("token", token));
        params.add(new BasicNameValuePair("cpt", String.valueOf(cpt)));
        params.add(new BasicNameValuePair("iPathId", String.valueOf(iPathId)));
        params.add(new BasicNameValuePair("startdate", startDate));
        params.add(new BasicNameValuePair("enddate", endDate));
        params.add(new BasicNameValuePair("adult", String.valueOf(adult)));
        params.add(new BasicNameValuePair("viewType", String.valueOf(viewType)));
        params.add(new BasicNameValuePair("offset", String.valueOf(offset)));
        params.add(new BasicNameValuePair("limit", String.valueOf(limit)));
        if(lat != 0){
            params.add(new BasicNameValuePair("lat", String.valueOf(lat)));
        }
        if(lng != 0){
            params.add(new BasicNameValuePair("lng", String.valueOf(lng)));
        }
        if(latBottom != 0){
            params.add(new BasicNameValuePair("latBottom", String.valueOf(latBottom)));
        }
        if(lngBottom != 0){
            params.add(new BasicNameValuePair("lngBottom", String.valueOf(lngBottom)));
        }
        if(latTop != 0){
            params.add(new BasicNameValuePair("latTop", String.valueOf(latTop)));
        }
        if(lngTop != 0){
            params.add(new BasicNameValuePair("lngTop", String.valueOf(lngTop)));
        }

        HttpHandler http = new HttpHandler(SERVER_IP + HOTEL_LIST_URL_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " hotelListUrl resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }

        return new JSONObject(resultJson);

    }

    public static JSONObject hotelListUrl(Context context, String token, int cpt, int iPathId,
                                          String startDate, String endDate, int adult, int offset, int limit, int viewType, double lat,
                                          double lng, double latBottom, double lngBottom, double latTop, double lngTop,
                                          int children, int rooms, int sort, double priceDown, double priceTop, String indexRange,
                                          String overallLiking, boolean specialOffer, String facilities) throws JSONException {
        if(token == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("token", token));
        params.add(new BasicNameValuePair("cpt", String.valueOf(cpt)));
        params.add(new BasicNameValuePair("iPathId", String.valueOf(iPathId)));
        params.add(new BasicNameValuePair("startdate", startDate));
        params.add(new BasicNameValuePair("enddate", endDate));
        params.add(new BasicNameValuePair("adult", String.valueOf(adult)));
        params.add(new BasicNameValuePair("viewType", String.valueOf(viewType)));
        params.add(new BasicNameValuePair("offset", String.valueOf(offset)));
        params.add(new BasicNameValuePair("limit", String.valueOf(limit)));


        if(lat != 0){
            params.add(new BasicNameValuePair("lat", String.valueOf(lat)));
        }
        if(lng != 0){
            params.add(new BasicNameValuePair("lng", String.valueOf(lng)));
        }
        if(latBottom != 0){
            params.add(new BasicNameValuePair("latBottom", String.valueOf(latBottom)));
        }
        if(lngBottom != 0){
            params.add(new BasicNameValuePair("lngBottom", String.valueOf(lngBottom)));
        }
        if(latTop != 0){
            params.add(new BasicNameValuePair("latTop", String.valueOf(latTop)));
        }
        if(lngTop != 0){
            params.add(new BasicNameValuePair("lngTop", String.valueOf(lngTop)));
        }



        if(children != 0){
            params.add(new BasicNameValuePair("children", String.valueOf(children)));
        }
        if(rooms != 0){
            params.add(new BasicNameValuePair("rooms", String.valueOf(rooms)));
        }
        if(sort != 0){
            params.add(new BasicNameValuePair("sort", String.valueOf(sort)));
        }
        if(priceTop != 0){
            params.add(new BasicNameValuePair("priceTop", String.valueOf(priceTop)));
        }
        if(priceDown != 0){
            params.add(new BasicNameValuePair("priceDown", String.valueOf(priceDown)));
        }
        if(indexRange != null){
            params.add(new BasicNameValuePair("indexRange", indexRange));
        }
        if(overallLiking != null){
            params.add(new BasicNameValuePair("overallLiking", overallLiking));
        }

        params.add(new BasicNameValuePair("specialOffer", String.valueOf(specialOffer)));

        if(facilities != null){
            params.add(new BasicNameValuePair("facilities", facilities));
        }


        HttpHandler http = new HttpHandler(SERVER_IP + HOTEL_LIST_URL_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " hotelListUrl resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }

        return new JSONObject(resultJson);

    }

    public static JSONObject hotelDetailUrl(Context context, String token, int hid, String locale) throws JSONException {
        if(token == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("token", token));
        params.add(new BasicNameValuePair("hid", String.valueOf(hid)));
        params.add(new BasicNameValuePair("locale", locale));

        HttpHandler http = new HttpHandler(SERVER_IP + HOTEL_DETAIL_URL_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " hotelDetailUrl resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }

        return new JSONObject(resultJson);

    }

    public static JSONObject hotelList(Context context, String token, String content) throws JSONException {
        if(token == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("token", token));
        params.add(new BasicNameValuePair("content", content));

        HttpHandler http = new HttpHandler(SERVER_IP + HOTEL_LIST_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " hotelList resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }

        return new JSONObject(resultJson);

    }

    public static JSONObject hotelDetail(Context context, String token, String infoContent, String imageContent, String ratingContent) throws JSONException {
        if(token == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("token", token));
        params.add(new BasicNameValuePair("infoContent", infoContent));
        params.add(new BasicNameValuePair("imageContent", imageContent));
        params.add(new BasicNameValuePair("ratingContent", ratingContent));

        HttpHandler http = new HttpHandler(SERVER_IP + HOTEL_DETAIL_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " hotelDetail resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }

        return new JSONObject(resultJson);

    }

    public static JSONObject hotelUrl(Context context, String token, String otaType, String id, String link, String startDate, String endDate,
                                      int roomType, int adult, int children, int rooms, int hid) throws JSONException {
        if(token == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("token", token));
        params.add(new BasicNameValuePair("otaType", otaType));
        params.add(new BasicNameValuePair("id", id));
        params.add(new BasicNameValuePair("link", link));
        params.add(new BasicNameValuePair("startdate", startDate));
        params.add(new BasicNameValuePair("enddate", endDate));

        if(roomType != 0){
            params.add(new BasicNameValuePair("roomType", String.valueOf(roomType)));
        }

        params.add(new BasicNameValuePair("adult", String.valueOf(adult)));
        params.add(new BasicNameValuePair("children", String.valueOf(children)));
        params.add(new BasicNameValuePair("rooms", String.valueOf(rooms)));
        params.add(new BasicNameValuePair("hid", String.valueOf(hid)));


        HttpHandler http = new HttpHandler(SERVER_IP + HOTEL_URL_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " hotelUrl resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }

        return new JSONObject(resultJson);

    }


    public static JSONObject yPoint(Context context, String token) throws JSONException {
        if(token == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("token", token));

        HttpHandler http = new HttpHandler(SERVER_IP + Y_POINT_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " yPoint resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }

        return new JSONObject(resultJson);

    }

    public static JSONObject martProductList(Context context, String token, String locale, int categoryId, String countryId) throws JSONException {
        if(token == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("token", token));
        params.add(new BasicNameValuePair("locale", locale));
        params.add(new BasicNameValuePair("categoryId", String.valueOf(categoryId) ));
        params.add(new BasicNameValuePair("countryId", countryId));

        HttpHandler http = new HttpHandler(SERVER_IP + MART_PRODUCT_LIST_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " martProductList resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }

        return new JSONObject(resultJson);

    }

    public static JSONObject martProduct(Context context, String token, int productId) throws JSONException {
        if(token == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("token", token));
        params.add(new BasicNameValuePair("productId", String.valueOf(productId) ));

        HttpHandler http = new HttpHandler(SERVER_IP + MART_PRODUCT_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " martProduct resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }

        return new JSONObject(resultJson);

    }

    public static JSONObject martPurchaseProduct(Context context, String token, int productId) throws JSONException {
        if(token == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("token", token));
        params.add(new BasicNameValuePair("productId", String.valueOf(productId) ));

        HttpHandler http = new HttpHandler(SERVER_IP + MART_PURCHASE_PRODUCT_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " martPurchaseProduct resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }

        return new JSONObject(resultJson);

    }

    public static JSONObject pocketList(Context context, String token) throws JSONException {
        if(token == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("token", token));

        HttpHandler http = new HttpHandler(SERVER_IP + POCKET_LIST_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " pocketList resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }

        return new JSONObject(resultJson);

    }

    public static JSONObject uploadPage(Context context, String token, String otaType, String url, String content) throws JSONException {
        if(token == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("token", token));
        params.add(new BasicNameValuePair("otaType", otaType));
        params.add(new BasicNameValuePair("url", url));
        params.add(new BasicNameValuePair("content", content));

        HttpHandler http = new HttpHandler(SERVER_IP + UPLOAD_PAGE_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " uploadPage resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }

        return new JSONObject(resultJson);

    }

    public static JSONObject requestRoaming(Context context, String token,
                                            int action, String roamingId, int roamingPlanId, int countryCode,
                                            String phone, long roamingStartTime) throws JSONException {
        if(token == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("token", token));
        params.add(new BasicNameValuePair("action", String.valueOf(action)));
        params.add(new BasicNameValuePair("roamingId", roamingId));
        params.add(new BasicNameValuePair("roamingPlanId", String.valueOf(roamingPlanId)));
        params.add(new BasicNameValuePair("countryCode", String.valueOf(countryCode)));
        params.add(new BasicNameValuePair("phone", phone));
        params.add(new BasicNameValuePair("roamingStartTime", String.valueOf(roamingStartTime)));

        HttpHandler http = new HttpHandler(SERVER_IP + REQUEST_ROAMING_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " requestRoaming resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }

        return new JSONObject(resultJson);

    }

    public static JSONObject userInfo(Context context, String token) throws JSONException {
        if(token == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("token", token));

        HttpHandler http = new HttpHandler(SERVER_IP + USER_INFO_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " userInfo resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }

        return new JSONObject(resultJson);

    }

    public static JSONObject waterRegister(Context context, String name, String password, String DoB, String email, String address,
                                           String telephone, String role, String website) throws JSONException {
        if(name == null)
            return null;

        String resultJson;
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("password", password));
        params.add(new BasicNameValuePair("DoB", DoB));
        params.add(new BasicNameValuePair("email", email));
        params.add(new BasicNameValuePair("address", address));
        params.add(new BasicNameValuePair("telephone", telephone));
        params.add(new BasicNameValuePair("role", role));
        params.add(new BasicNameValuePair("website", website));


        HttpHandler http = new HttpHandler(WATER_SERVER_IP + WATER_REGISTER_PATH, params);
        resultJson = http.getResponseByPostJsonBody();
        LOG.D(TAG, " waterRegister resultJson = " + resultJson);

        if(resultJson == null){
            return null;
        }

        return new JSONObject(resultJson);

    }



}
