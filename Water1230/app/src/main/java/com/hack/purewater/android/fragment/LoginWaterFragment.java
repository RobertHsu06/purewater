package com.hack.purewater.android.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.hack.purewater.android.LandingPageActivity;
import com.hack.purewater.android.R;
import com.hack.purewater.android.util.Constants;
import com.hack.purewater.android.util.LOG;
import com.hack.purewater.android.util.WaterUtils;


/**
 * Created by Giles on 2017/12/24.
 */

public class LoginWaterFragment extends BaseFragment {
    public static final String TAG = "LoginWaterFragment";

    private View mView = null;
    private Context mContext = null;
    private LandingPageActivity mLandingPageActivity

            ;
    private FragmentActivity mActivity = null;
    //    private NetworkManager mNetworkManager = null;
    private Boolean mCurrentNetworkAvaliable;

    private Button mBtnLogin = null;
    private Button mBtnRegister = null;

    private RegisterWaterFragment mRegisterWaterFragment = null;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LOG.D(TAG,"onCreate");
        mActivity = getActivity();
        mContext = getActivity();
        mLandingPageActivity = (LandingPageActivity) getActivity();


        Bundle bundle = getArguments();
        if(bundle != null){
//            mPageFrom = bundle.getInt(IndoorMapFragment.KEY_PAGE_FROM, -1);
            bundle.clear();
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LOG.V(TAG, "[onCreateView]  mView = " + mView);

        if (mView == null) {
            mView = inflater.inflate(R.layout.login_water_fragment, null);
        } else {
            ViewGroup parent = (ViewGroup) mView.getParent();
            if (parent != null) {
                parent.removeView(mView);
            }
        }
        initView(mView);
        return mView;
    }

    @Override
    public void onStart() {
        super.onStart();
        LOG.D(TAG,"onStart");
    }

    @Override
    public void onResume(){
        super.onResume();
        LOG.D(TAG,"onResume");

//        mLandingPageActivity.setActionBarBackShow(false);
//        mLandingPageActivity.setActionBarShow(true);
//        mLandingPageActivity.setActionBarTitle(getString(R.string.txt_title_my));
//        mLandingPageActivity.setFooterBarShow(true);

    }

    @Override
    public void onStop() {
        super.onStop();

        LOG.V(TAG, "onStop() - start ");

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        LOG.V(TAG, "onDestroy() - start ");
    }

    private void initView(View view){

        mBtnLogin = (Button) mView.findViewById(R.id.btn_login);
        mBtnRegister = (Button) mView.findViewById(R.id.btn_register);


        mBtnLogin.setOnClickListener(mBtnLoginClickListener);
        mBtnRegister.setOnClickListener(mBtnRegisterClickListener);



    }


    private void launchFragment(int tag, boolean removeChooseList) {
        LOG.V(TAG, "[launchFragment] tag = " + tag);


        FragmentManager fm = getFragmentManager();
        //FragmentManager fm = getChildFragmentManager();
        if(fm == null) {
            LOG.W(TAG, "[launchFragment] FragmentManager is null.");
            return;
        }

        FragmentTransaction ft = fm.beginTransaction();
        if(ft == null) {
            LOG.W(TAG, "[launchFragment] FragmentTransaction is null.");
            return;
        }

        Bundle arguments= new Bundle();

        switch (tag) {

            case Constants.FRAGMENT_TAG_WATER_REGISTER:
                mRegisterWaterFragment = new RegisterWaterFragment();

//                arguments.putString(WebViewFragment.KEY_WEB_VIEW_URL, webViewUrl);
//                mHomeFragment.setArguments(arguments);

                ft.replace(Constants.LANDING_FRAME_ID, mRegisterWaterFragment, RegisterWaterFragment.TAG).addToBackStack(RegisterWaterFragment.TAG).commitAllowingStateLoss();
                break;


        }

    }



    private Button.OnClickListener mBtnLoginClickListener = new Button.OnClickListener() {
        public void onClick(View v) {
            LOG.D(TAG,"mBtnLoginClickListener");
            //goto main activity
            mLandingPageActivity.goToWaterMainActivity();


        }
    };

    private Button.OnClickListener mBtnRegisterClickListener = new Button.OnClickListener() {
        public void onClick(View v) {
            LOG.D(TAG,"mBtnRegisterClickListener");
            //goto main activity
            launchFragment(Constants.FRAGMENT_TAG_WATER_REGISTER, false);

        }
    };



}
