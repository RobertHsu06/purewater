package com.hack.purewater.android.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.maps.LocationSource;
import com.hack.purewater.android.util.LOG;

/**
 * Created by ggshao on 2017/7/27.
 */

public class AndroidLocationService extends Service
{
    private static final String TAG = "AndroidLocationService";
    private LocationManager mLocationManager = null;
    //    private static final int LOCATION_INTERVAL = 20000;
//    private static final int LOCATION_INTERVAL = 30000;
        private static final int LOCATION_INTERVAL = 1000;
//    private static final int LOCATION_INTERVAL = 0;
    private static final float LOCATION_DISTANCE = 0f;
//    private static final long UPDATE_GPS_INTERVAL = 300000;//5 minutes
//    private static final long UPDATE_GPS_INTERVAL = 30000;//30 seconds
    private static final long UPDATE_GPS_INTERVAL = 200;//5 seconds

    private LocalBroadcastManager mBroadcaster;

    public static final String GPS_LOCATION_CHANGE = "com.gorilla.psa.emergency.service.gps.location.change";

    private long mGpsUpdateTimeMillis = 0;
    private boolean mIsFirstLaunchGps = false;

    private class LocationListener implements LocationSource, android.location.LocationListener
    {
        private OnLocationChangedListener mListener = null;
        Location mLastLocation;

        public LocationListener(String provider)
        {
            LOG.D(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void activate(OnLocationChangedListener pListener) {
            LOG.D(TAG,"activate");
            //When the setLocationSource is called on the map, the map
            //will be passed in as an OnLocationChangedListener (may not be the
            //map, but may be a delegator created by the map.
            mListener = pListener;
        }

        @Override
        public void deactivate() {
            LOG.D(TAG,"deactivate");
            mListener = null;

        }

        private void notifyListener() {

            if(mListener != null && mLastLocation!=null) {

//                if(mLastHeading != Float.MAX_VALUE) {
//                    mLastLocation.setBearing(mLastHeading);
//                }

                //The listener will be the maps:
                mListener.onLocationChanged(mLastLocation);

            }

        }

        @Override
        public void onLocationChanged(Location location)
        {
            LOG.D(TAG, "onLocationChanged: " + location);

//            mLastLocation.set(location);
//            notifyListener();



            Double longitude = location.getLongitude();  //得到经度
            Double latitude = location.getLatitude();

            LOG.D(TAG,"onLocationChanged latitude = " + latitude);
            LOG.D(TAG,"onLocationChanged longitude = " + longitude);

        }

        @Override
        public void onProviderDisabled(String provider)
        {

            LOG.D(TAG, "onProviderDisabled: " + provider);
            mIsFirstLaunchGps = false;

            //GPS DONE
            Intent intent = new Intent(GPS_LOCATION_CHANGE);
            mBroadcaster.sendBroadcast(intent);


        }

        @Override
        public void onProviderEnabled(String provider)
        {
            LOG.D(TAG, "onProviderEnabled: " + provider);
            mIsFirstLaunchGps = true;




        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras)
        {
            LOG.D(TAG, "onStatusChanged: " + provider);
        }
    }

    LocationListener[] mLocationListeners = new LocationListener[] {
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };

    @Override
    public IBinder onBind(Intent arg0)
    {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        LOG.D(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onCreate()
    {
        LOG.D(TAG, "onCreate");
        initializeLocationManager();
        mBroadcaster = LocalBroadcastManager.getInstance(this);


        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);
        } catch (java.lang.SecurityException ex) {
            LOG.D(TAG, "fail to request location update, ignore");
        } catch (IllegalArgumentException ex) {
            LOG.D(TAG, "network provider does not exist, " + ex.getMessage());
        }


        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (java.lang.SecurityException ex) {
            LOG.D(TAG, "fail to request location update, ignore");
        } catch (IllegalArgumentException ex) {
            LOG.D(TAG, "gps provider does not exist " + ex.getMessage());
        }
    }

    @Override
    public void onDestroy()
    {
        LOG.D(TAG, "onDestroy");
        super.onDestroy();
        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (SecurityException ex) {
                    LOG.D(TAG, "fail to remove location listners, ignore");
                }
            }
        }

        mGpsUpdateTimeMillis = 0;
    }

    private void initializeLocationManager() {
        LOG.D(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

}
