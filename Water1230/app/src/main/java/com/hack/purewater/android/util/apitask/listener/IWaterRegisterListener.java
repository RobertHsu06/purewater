package com.hack.purewater.android.util.apitask.listener;

import com.hack.purewater.android.dataModel.WaterRegisterModel;

/**
 * Created by Giles on 2017/12/25.
 */

public interface IWaterRegisterListener {
    public void onWaterRegister(WaterRegisterModel model);
}
