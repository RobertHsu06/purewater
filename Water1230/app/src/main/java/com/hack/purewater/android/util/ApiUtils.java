package com.hack.purewater.android.util;

import android.content.Context;
import android.os.AsyncTask;


import com.hack.purewater.android.util.apitask.UserInfoTask;
import com.hack.purewater.android.util.apitask.WaterRegisterTask;

import com.hack.purewater.android.util.apitask.listener.IUserInfoListener;
import com.hack.purewater.android.util.apitask.listener.IWaterRegisterListener;

import java.util.ArrayList;
import java.util.Iterator;

import static android.content.ContentValues.TAG;

/**
 * Created by ggshao on 2017/7/17.
 */

public class ApiUtils {
    public static class TagAsyncTask {
        String tag;
        AsyncTask task;

        public TagAsyncTask(String tag, AsyncTask task)
        {
            this.tag = tag;
            this.task = task;
        }

        public String getTag() {
            return tag;
        }

        public void setTag(String tag) {
            this.tag = tag;
        }

        public AsyncTask getTask() {
            return task;
        }

        public void setTask(AsyncTask task) {
            this.task = task;
        }

    }

    private static ArrayList<TagAsyncTask> taskList;

    static {
        if(taskList == null)
            taskList = new ArrayList<TagAsyncTask>();
    }

    public static void addTagTask(String tag, AsyncTask task)
    {
        TagAsyncTask tagTask = new TagAsyncTask(tag, task);
        synchronized(taskList)
        {
            taskList.add(tagTask);
        }
    }

    public static void cancelTaskByTag(String tag)
    {
        Iterator<TagAsyncTask> iter = taskList.iterator();
        synchronized(taskList)
        {
            while (iter.hasNext()) {
                TagAsyncTask task = iter.next();

                if(tag.equals(task.getTag()))
                {
                    if (task.getTask() != null)
                        task.getTask().cancel(true);
                    iter.remove();
                }
            }
        }
    }


    public static UserInfoTask userInfo(String tag, Context context, String token, IUserInfoListener callback)
    {
        LOG.D(TAG,"userInfo token = " + token);
        UserInfoTask task = new UserInfoTask();
        WaterUtils.executeAsyncTask(task, context, token, callback);
        addTagTask(tag, task);
        return task;
    }


    public static WaterRegisterTask waterRegister(String tag, Context context, String name, String password, String DoB, String email, String address,
                                                  String telephone, String role, String website, IWaterRegisterListener callback)
    {
        LOG.D(TAG,"waterRegister name = " + name);
        WaterRegisterTask task = new WaterRegisterTask();
        WaterUtils.executeAsyncTask(task, context, name, password, DoB, email, address, telephone, role, website, callback);
        addTagTask(tag, task);
        return task;
    }


}
