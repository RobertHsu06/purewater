package com.hack.purewater.android.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hack.purewater.android.R;
import com.hack.purewater.android.util.LOG;
import com.hack.purewater.android.util.apitask.listener.IButtonClickListener;

/**
 * Created by ggshao on 2017/12/4.
 */

public class MessageDialog extends PopupDialog{

    /*
     * ======================================================================
     * Constant Fields
     * =======================================================================
     */
//    public static final String TAG = "MessageDialog";
//
//    private Context mContext;
//    private MainActivity mMainActivity = null;
//    private String mErrorContent = null;
//
//    private ImageView mImgClose = null;
//    private TextView mTxtMessage = null;
//    private Button mBtnClose = null;
//    private boolean mIsBack = false;
//
//    private IButtonClickListener mCallback = null;
//


    public MessageDialog(Context context){
        super(context);
    }


//    public MessageDialog(Context context, MainActivity mainActivity, String message, boolean isBack, IButtonClickListener callback) {
//        super(context);
//
//        this.mContext = context;
//        mCallback = callback;
//        mMainActivity = mainActivity;
//        mIsBack = isBack;
//        setContentView(R.layout.dialog_message);
//
////        setCanceledOnTouchOutside(false);
//        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
//
//        mImgClose = (ImageView) this.findViewById(R.id.img_close);
//        mTxtMessage = (TextView) this.findViewById(R.id.txt_message);
//        mBtnClose = (Button) this.findViewById(R.id.btn_close);
//
//        mImgClose.setOnClickListener(mImgCloseClickListener);
//        mTxtMessage.setText(message);
//        mBtnClose.setOnClickListener(mImgCloseClickListener);
//
//    }
//
//
//    @Override
//    public void onDismiss(DialogInterface dialogInterface) {
//        LOG.D(TAG,"onDismiss");
//
//    }
//
//    @Override
//    public void setOnDismissListener(OnDismissListener listener) {
//        LOG.D(TAG,"setOnDismissListener ");
//        super.setOnDismissListener(listener);
//
//    }
//
//
//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//        LOG.D(TAG,"onTouchEvent event = " + event);
//        return super.onTouchEvent(event);
//
//
//    }
//
//    private ImageView.OnClickListener mImgCloseClickListener = new ImageView.OnClickListener(){
//
//        @Override
//        public void onClick(View v) {
//            LOG.D(TAG,"mImgCloseClickListener mIsBack = " + mIsBack);
////            mMainActivity.backToHome();
//            dismiss();
//            if(mIsBack){
//                //popBack
//                mCallback.onButtonClicked(0, null, null);
//            }else{
//
//            }
//        }
//    };





}
