package com.hack.purewater.android.util.apitask;

import android.content.Context;
import android.os.AsyncTask;

import com.hack.purewater.android.dataModel.UserInfoModel;
import com.hack.purewater.android.util.ApiAccessor;
import com.hack.purewater.android.util.ApiResultParser;
import com.hack.purewater.android.util.LOG;
import com.hack.purewater.android.util.apitask.listener.IUserInfoListener;

/**
 * Created by ggshao on 2017/11/30.
 */

public class UserInfoTask extends AsyncTask<Object, Integer, UserInfoModel> {
    public static final String TAG = "UserInfoTask";

    IUserInfoListener callback;

    @Override
    protected UserInfoModel doInBackground(Object... params) {
        Context context = (Context) params[0];
        String token = (String) params[1];
        callback = (IUserInfoListener) params[2];

        UserInfoModel model = null;

        try {

//            model = ApiResultParser.loginDataParser(ApiAccessor.login(context, phoneNumber));
            model = ApiResultParser.userInfoParser(ApiAccessor.userInfo(context, token));

        } catch (Throwable tr) {
            LOG.E(TAG, "GetAccessTokenTask() - failed.", tr);
        }

        return model;
    }

    @Override
    protected void onPostExecute(UserInfoModel result) {

        if(callback != null)
            callback.onUserInfo(result);
    }
}
