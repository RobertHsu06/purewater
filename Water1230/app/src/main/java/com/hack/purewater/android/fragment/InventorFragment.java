package com.hack.purewater.android.fragment;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.hack.purewater.android.LandingPageActivity;
import com.hack.purewater.android.R;
import com.hack.purewater.android.WaterMainActivity;
import com.hack.purewater.android.util.Constants;
import com.hack.purewater.android.util.LOG;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Giles on 2017/12/25.
 */

public class InventorFragment extends BaseFragment {
    public static final String TAG = "InventorFragment";

    private View mView = null;
    private Context mContext = null;
    private WaterMainActivity mWaterMainActivity;
    private FragmentActivity mActivity = null;
    //    private NetworkManager mNetworkManager = null;
    private Boolean mCurrentNetworkAvaliable;

    private Button mBtnLogin = null;
    private Button mBtnRegister = null;

    private WaterProjectInfoFragment mWaterProjectInfoFragment = null;
    private WaterAccountInfoFragment mWaterAccountInfoFragment = null;


    private ViewPager mViewPager = null;
    private Adapter mAdapter = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LOG.D(TAG,"onCreate");
        mActivity = getActivity();
        mContext = getActivity();
        mWaterMainActivity = (WaterMainActivity) getActivity();


        Bundle bundle = getArguments();
        if(bundle != null){
//            mPageFrom = bundle.getInt(IndoorMapFragment.KEY_PAGE_FROM, -1);
            bundle.clear();
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LOG.V(TAG, "[onCreateView]  mView = " + mView);

        if (mView == null) {
            mView = inflater.inflate(R.layout.inventor_fragment, null);
        } else {
            ViewGroup parent = (ViewGroup) mView.getParent();
            if (parent != null) {
                parent.removeView(mView);
            }
        }
        initView(mView);
        return mView;
    }

    @Override
    public void onStart() {
        super.onStart();
        LOG.D(TAG,"onStart");
    }

    @Override
    public void onResume(){
        super.onResume();
        LOG.D(TAG,"onResume");

//        mLandingPageActivity.setActionBarBackShow(false);
//        mLandingPageActivity.setActionBarShow(true);
//        mLandingPageActivity.setActionBarTitle(getString(R.string.txt_title_my));
//        mLandingPageActivity.setFooterBarShow(true);

    }

    @Override
    public void onStop() {
        super.onStop();

        LOG.V(TAG, "onStop() - start ");

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        LOG.V(TAG, "onDestroy() - start ");

        mViewPager = null;
        mAdapter = null;
        mView = null;
    }

    private void initView(View view){

//        mBtnLogin = (Button) mView.findViewById(R.id.btn_login);
//        mBtnRegister = (Button) mView.findViewById(R.id.btn_register);
//
//
//        mBtnLogin.setOnClickListener(mBtnLoginClickListener);
//        mBtnRegister.setOnClickListener(mBtnRegisterClickListener);


        mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
        setupViewPager(mViewPager);
        // Set Tabs inside Toolbar
        TabLayout tabs = (TabLayout) view.findViewById(R.id.result_tabs);
        tabs.setupWithViewPager(mViewPager);

        LinearLayout linearLayout = (LinearLayout)tabs.getChildAt(0);
        linearLayout.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(mContext.getResources().getColor(R.color.white_grey));
        drawable.setSize(2, 2);
//        linearLayout.setDividerPadding(10);
        linearLayout.setDividerDrawable(drawable);

    }

    private void setupViewPager(ViewPager viewPager) {
        LOG.D(TAG,"setupViewPager viewPager = " + viewPager );

        if(mWaterProjectInfoFragment == null){
            mWaterProjectInfoFragment = new WaterProjectInfoFragment();
            mWaterAccountInfoFragment = new WaterAccountInfoFragment();
        }


//        mPocketReservationFragment.setFragmentManager(getFragmentManager());

        mAdapter = new Adapter(getChildFragmentManager());
        mAdapter.addFragment(mWaterProjectInfoFragment, mContext.getString(R.string.txt_project_info));
        mAdapter.addFragment(mWaterAccountInfoFragment, mContext.getString(R.string.txt_account_info));
        viewPager.setAdapter(mAdapter);


        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, final float v, final int i2) {
            }

            @Override
            public void onPageSelected(final int position) {
                LOG.D(TAG,"onPageSelected position = " + position);


            }

            @Override
            public void onPageScrollStateChanged(final int position) {
                LOG.D(TAG,"onPageScrollStateChanged position = " + position);
            }
        });



    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    private void launchFragment(int tag, boolean removeChooseList) {
        LOG.V(TAG, "[launchFragment] tag = " + tag);


        FragmentManager fm = getFragmentManager();
        //FragmentManager fm = getChildFragmentManager();
        if(fm == null) {
            LOG.W(TAG, "[launchFragment] FragmentManager is null.");
            return;
        }

        FragmentTransaction ft = fm.beginTransaction();
        if(ft == null) {
            LOG.W(TAG, "[launchFragment] FragmentTransaction is null.");
            return;
        }

        Bundle arguments= new Bundle();

        switch (tag) {

//            case Constants.FRAGMENT_TAG_WATER_REGISTER:
//                mRegisterWaterFragment = new RegisterWaterFragment();
//
////                arguments.putString(WebViewFragment.KEY_WEB_VIEW_URL, webViewUrl);
////                mHomeFragment.setArguments(arguments);
//
//                ft.replace(Constants.LANDING_FRAME_ID, mRegisterWaterFragment, RegisterWaterFragment.TAG).addToBackStack(RegisterWaterFragment.TAG).commitAllowingStateLoss();
//                break;


        }

    }




}
