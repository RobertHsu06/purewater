package com.hack.purewater.android.util;


import com.hack.purewater.android.dataModel.UserInfoModel;
import com.hack.purewater.android.dataModel.WaterRegisterModel;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by ggshao on 2017/7/17.
 */

public class ApiResultParser {
    private static final String TAG = "ApiResultParser";




    public static UserInfoModel userInfoParser(JSONObject obj) {
        LOG.D(TAG,"userInfoParser");
        if (obj == null) {
            return null;
        }

        LOG.D(TAG,"userInfoParser obj = " + obj);
        LOG.D(TAG,"userInfoParser obj.toString() = " + obj.toString());

        UserInfoModel model = new UserInfoModel();

        ObjectMapper objectMapper = new ObjectMapper();
        try{
            model = objectMapper.readValue(obj.toString(), UserInfoModel.class);
        }catch (IOException e){
            LOG.D(TAG, "userInfoParser IOException e = " + e);
            e.printStackTrace();
        }

        return model;

    }


    public static WaterRegisterModel waterRegisterParser(JSONObject obj) {
        LOG.D(TAG,"waterRegisterParser");
        if (obj == null) {
            return null;
        }

        LOG.D(TAG,"waterRegisterParser obj = " + obj);
        LOG.D(TAG,"waterRegisterParser obj.toString() = " + obj.toString());

        WaterRegisterModel model = new WaterRegisterModel();

        ObjectMapper objectMapper = new ObjectMapper();
        try{
            model = objectMapper.readValue(obj.toString(), WaterRegisterModel.class);
        }catch (IOException e){
            LOG.D(TAG, "waterRegisterParser IOException e = " + e);
            e.printStackTrace();
        }

        return model;

    }


    //----------------------------------------data parser start--------------------------------








































//    public static GetAccessTokenModel getAccessTokenDataParser(JSONObject obj) {
//        LOG.D(TAG,"getAccessTokenDataParser");
//        if (obj == null) {
//            return null;
//        }
//
//        LOG.D(TAG,"getAccessTokenDataParser obj = " + obj);
//        LOG.D(TAG,"getAccessTokenDataParser obj.toString() = " + obj.toString());
//
//        GetAccessTokenModel model = new GetAccessTokenModel();
//
//        try {
//
//            if (!obj.isNull(GetAccessTokenModel.KEY_CODE))
//                model.setCode(obj.getString(GetAccessTokenModel.KEY_CODE));
//
////            if (!obj.isNull(GetConfigAreaModel.KEY_STATUS)){
////                JSONObject statusJsonObj = new JSONObject(obj.getString(GetConfigAreaModel.KEY_STATUS));
////                model.setStatus(statusParser(statusJsonObj));
////            }
//            if (!obj.isNull(GetAccessTokenModel.KEY_DATA)){
//                JSONObject dataJsonObj = new JSONObject(obj.getString(GetAccessTokenModel.KEY_DATA));
//                model.setData(dataParser(dataJsonObj));
//            }
//
//
//        } catch (Throwable tr) {
//            LOG.E(TAG, "getVideoPlayListDataParser() - failed.", tr);
//        }
//
//        return model;
//
//    }
//
//    private static DataModel dataParser(JSONObject dataJsonObj) {
//        DataModel dataModel = new DataModel();
//        try{
//            if (!dataJsonObj.isNull(DataModel.KEY_TYPE))
//                dataModel.setType(dataJsonObj.getString(DataModel.KEY_TYPE));
//
//            if (!dataJsonObj.isNull(DataModel.KEY_U_NAME))
//                dataModel.setUname(dataJsonObj.getString(DataModel.KEY_U_NAME));
//
//            if (!dataJsonObj.isNull(DataModel.KEY_TOKEN))
//                dataModel.setToken(dataJsonObj.getString(DataModel.KEY_TOKEN));
//
//        } catch (JSONException e) {
//            LOG.E(TAG, "JSONException occurs, ", e);
//            return null;
//        }
//
//        return dataModel;
//    }
//
//    public static GetRouteFromGoogleModel getRouteFromGoogleDataParser(JSONObject obj) {
//
//        if (obj == null) {
//            return null;
//        }
//
//        GetRouteFromGoogleModel getRouteFromGoogleModel = new GetRouteFromGoogleModel();
//        try{
//
//            if (!obj.isNull(GetRouteFromGoogleModel.KEY_ROUTES)){
//                JSONArray routesArray = new JSONArray(obj.getString(GetRouteFromGoogleModel.KEY_ROUTES));
//                LOG.D(TAG,"routesArray.length() = " + routesArray.length());
//                for(int i = 0 ; i < routesArray.length() ; i++){
//                    JSONObject routesJsonObj = routesArray.getJSONObject(i);
//                    getRouteFromGoogleModel.addRoutes(routesParser(routesJsonObj));
//                }
//            }
//
//
//
//        } catch (JSONException e) {
//            LOG.E(TAG, "JSONException occurs, ", e);
//            return null;
//        }
////
//        return getRouteFromGoogleModel;
//
//    }
//
//
//    public static RoutesModel routesParser(JSONObject obj) {
//
//        if (obj == null) {
//            return null;
//        }
//
//        RoutesModel routesModel = new RoutesModel();
//        try{
//
//            if (!obj.isNull(RoutesModel.KEY_LEGS)){
//                JSONArray legsArray = new JSONArray(obj.getString(RoutesModel.KEY_LEGS));
//                LOG.D(TAG,"legsArray.length() = " + legsArray.length());
//                for(int i = 0 ; i < legsArray.length() ; i++){
//                    JSONObject legsJsonObj = legsArray.getJSONObject(i);
//                    routesModel.addLegs(legsParser(legsJsonObj));
//                }
//            }
//
//        } catch (JSONException e) {
//            LOG.E(TAG, "JSONException occurs, ", e);
//            return null;
//        }
////
//        return routesModel;
//
//    }
//
//    public static LegsModel legsParser(JSONObject obj) {
//
//        if (obj == null) {
//            return null;
//        }
//
//        LegsModel legsModel = new LegsModel();
//        try{
//
//            if (!obj.isNull(LegsModel.KEY_STEPS)){
//                JSONArray stepsArray = new JSONArray(obj.getString(LegsModel.KEY_STEPS));
//                LOG.D(TAG,"stepsArray.length() = " + stepsArray.length());
//                for(int i = 0 ; i < stepsArray.length() ; i++){
//                    JSONObject stepsJsonObj = stepsArray.getJSONObject(i);
//                    legsModel.addSteps(stepsParser(stepsJsonObj));
//                }
//            }
//
//        } catch (JSONException e) {
//            LOG.E(TAG, "JSONException occurs, ", e);
//            return null;
//        }
////
//        return legsModel;
//
//    }
//
//    public static StepsModel stepsParser(JSONObject obj) {
//
//        if (obj == null) {
//            return null;
//        }
//
//        StepsModel stepsModel = new StepsModel();
//        try{
//
//            if (!obj.isNull(StepsModel.KEY_POLYLINE)){
//                JSONObject polylineJsonObj = new JSONObject(obj.getString(StepsModel.KEY_POLYLINE));
//                stepsModel.setPolyline(polylineParser(polylineJsonObj));
//            }
//
//        } catch (JSONException e) {
//            LOG.E(TAG, "JSONException occurs, ", e);
//            return null;
//        }
////
//        return stepsModel;
//
//    }
//
//    public static PolylineModel polylineParser(JSONObject obj) {
//
//        if (obj == null) {
//            return null;
//        }
//
//        PolylineModel stepsModel = new PolylineModel();
//        try{
//            if (!obj.isNull(PolylineModel.KEY_POINTS))
//                stepsModel.setPoints(obj.getString(PolylineModel.KEY_POINTS));
//
//        } catch (JSONException e) {
//            LOG.E(TAG, "JSONException occurs, ", e);
//            return null;
//        }
////
//        return stepsModel;
//
//    }

}
