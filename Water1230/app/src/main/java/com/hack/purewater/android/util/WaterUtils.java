package com.hack.purewater.android.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;

import com.hack.purewater.android.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.util.concurrent.RejectedExecutionException;

/**
 * Created by ggshao on 2017/7/17.
 */

public class WaterUtils {
    private static final String TAG = "WaterUtils";

    private static String mLoginToken = null;

    public static <T> void executeAsyncTask(AsyncTask<T, ?, ?> asyncTask, T... params) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
            else
                asyncTask.execute(params);
        }catch (RejectedExecutionException localRejectedExecutionException){
            Log.w(TAG,"localRejectedExecutionException");
        }
    }

    public static void setLoginToken(String token){
        mLoginToken = token;
    }

    public static String getLoginToken(){
        return mLoginToken;
    }


    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        LOG.D(TAG,"collapse initialHeight = " + initialHeight);

        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1){
                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }


    public static DisplayImageOptions getCustomDisplayImageOptions(int resId)
    {
        DisplayImageOptions displayOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(resId) // resource or drawable
                .showImageForEmptyUri(resId) // resource or drawable
                .showImageOnFail(resId) // resource or drawable
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(500))
                .build();

        return displayOptions;
    }






}
