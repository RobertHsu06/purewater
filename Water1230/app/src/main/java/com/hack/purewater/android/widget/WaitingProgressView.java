package com.hack.purewater.android.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.RelativeLayout;

import com.hack.purewater.android.R;

/**
 * Created by ggshao on 2017/7/27.
 */

public class WaitingProgressView extends RelativeLayout {
    private final static int TIME = 500;
    private final static int STATUS_FADE_IN = 0;
    private final static int STATUS_FADE_OUT = 1;
    private final static int STATUS_NORMAL = 2;
    private LayoutInflater mInflater;
    private String mLock;
    private int mStatus = STATUS_NORMAL;
    private float mBaseAlpha;
    public WaitingProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mInflater.inflate(R.layout.waiting_progress_layout, this);
        mLock = new String("WaitingProgressView-lock");
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    public void setVisiableImmediate(int visibility){
        synchronized (mLock){
            Animation currentAnimat = getAnimation();
            if(currentAnimat!=null)
                getAnimation().cancel();
            clearAnimation();
            mStatus = STATUS_NORMAL;
            if(visibility==View.VISIBLE)
                setAlpha(1F);
            setVisibility(visibility);
        }

    }


    private Animation creatNewAnimation() {
        Animation ani = new MyAnimation(getContext(),null){
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                synchronized (mLock) {
                    if(getIsCanceled() || getIsComplete())
                        return;
                    if (mStatus == STATUS_FADE_OUT) {
                        setAlpha(mBaseAlpha - mBaseAlpha * interpolatedTime);
                    } else if(mStatus== STATUS_FADE_IN){
                        setAlpha((1-mBaseAlpha )* interpolatedTime+mBaseAlpha);
                    }
                }
            }

        };
        return ani;
    }
    public void setVisiableWithAnimate(final int visiable){
        synchronized (mLock) {
            if (mStatus == STATUS_FADE_IN || mStatus == STATUS_NORMAL) {
                if (visiable == View.VISIBLE && getVisibility() == View.VISIBLE)
                    return;
            }

            boolean isRequestInvisiable = visiable == INVISIBLE || visiable == GONE;

            if (mStatus == STATUS_FADE_OUT || mStatus == STATUS_NORMAL) {
                if (isRequestInvisiable && (getVisibility() == View.INVISIBLE || getVisibility() == GONE))
                    return;
            }

            final boolean isNeedFadeIn;

            if (visiable == GONE || visiable == INVISIBLE) {
                isNeedFadeIn = false;
            } else {
                isNeedFadeIn = true;
            }


            if (mStatus == STATUS_FADE_OUT || mStatus == STATUS_FADE_IN) {
                Animation currentAnimat = getAnimation();
                if(currentAnimat!=null) {
                    currentAnimat.cancel();
                }
                clearAnimation();
            }else{
                setAlpha(isNeedFadeIn?0:1);
            }
            mBaseAlpha = getAlpha();
            Animation ani = creatNewAnimation();
            long time = (long) (TIME*(Math.abs(mBaseAlpha-(isNeedFadeIn?1:0))));
            ani.setDuration(time);
            mStatus = isNeedFadeIn ? STATUS_FADE_IN : STATUS_FADE_OUT;
            setVisibility(View.VISIBLE);
            ani.setAnimationListener(new Animation.AnimationListener() {

                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    synchronized (mLock){
                        if(((MyAnimation)animation).getIsCanceled()){
                            return;
                        }
                        if(mStatus==STATUS_FADE_OUT){
                            setVisibility(View.GONE);
                        }else{
                            setAlpha(1.0F);
                        }
                        mStatus = STATUS_NORMAL;
                        ((MyAnimation)animation).onComplete();
                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            startAnimation(ani);
        }
    }

    public class MyAnimation extends Animation{
        public MyAnimation(Context context,AttributeSet attributeSet){
            super(context,attributeSet);
        }
        private boolean isCompleted = false;
        private boolean isCanceled = false;
        public boolean getIsCanceled(){
            return isCanceled;
        }
        public void onComplete(){
            isCompleted = true;
        }
        public boolean getIsComplete(){
            return isCompleted;
        }
        @Override
        public void cancel() {
            isCanceled = true;
            isCompleted = true;
            super.cancel();
        }
    }
}
