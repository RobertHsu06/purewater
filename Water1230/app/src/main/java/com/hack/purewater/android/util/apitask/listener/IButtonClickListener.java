package com.hack.purewater.android.util.apitask.listener;

/**
 * Created by Giles on 2017/12/25.
 */

public interface IButtonClickListener {
    public void onButtonClicked(int buttonId, String viewName, Object[] args);
}
