package com.hack.purewater.android.util;

import com.hack.purewater.android.R;

/**
 * Created by ggshao on 2017/7/14.
 */

public class Constants {

    public final static int LANDING_FRAME_ID = R.id.landing_frame;
    public final static int WATER_CONTENT_FRAME_ID = R.id.water_content_frame;



    // ==================== For Fragment identify ================================
    public static final int FRAGMENT_TAG_SEARCH = 0;

    public static final int FRAGMENT_TAG_WATER_LOGIN = FRAGMENT_TAG_SEARCH + 1001;
    public static final int FRAGMENT_TAG_WATER_REGISTER = FRAGMENT_TAG_SEARCH + 1002;
    public static final int FRAGMENT_TAG_INVENTOR_FRAGMENT = FRAGMENT_TAG_SEARCH + 1003;


    public static final int ERROR_CODE_TOKEN_ERROR = 101;
    public static final int ERROR_CODE_TOKEN_EXPIRED = 102;
    public static final int ERROR_CODE_NO_TOKEN = 103;




    // ==================== For sharePref  ================================
    public static final String PREF_NAME_PURE_WATER = "pureWaterPref";

    public static final String PREF_KEY_IS_LOGIN = "key-is-login";




}
