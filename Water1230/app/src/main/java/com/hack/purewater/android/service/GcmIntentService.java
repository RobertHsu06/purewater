package com.hack.purewater.android.service;

import android.app.ActivityManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.hack.purewater.android.R;
import com.hack.purewater.android.receiver.GcmBroadcastReceiver;
import com.hack.purewater.android.util.LOG;

/**
 * Created by ggshao on 2017/12/4.
 */

public class GcmIntentService extends IntentService {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;

//	private static final String

    public GcmIntentService() {
        super("GcmIntentService");
    }

    public static final String TAG = "GcmIntentService";

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) { // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that
			 * GCM will be extended in the future with new message types, just
			 * ignore any message types you're not interested in, or that you
			 * don't recognize.
			 */
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
                    .equals(messageType)) {
                // sendNotification("Send error: " + extras.toString());
                LOG.V(TAG, "Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
                    .equals(messageType)) {
                // sendNotification("Deleted messages on server: " +
                // extras.toString());
                // If it's a regular GCM message, do some work.
                LOG.V(TAG, "Deleted messages on server: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
                    .equals(messageType)) {
                LOG.V(TAG, "Received: " + extras.toString());
                String message = intent.getStringExtra("message");
                String title = intent.getStringExtra("title");

                LOG.V(TAG, "Received: message = " + message + " title = " + title);

                Toast.makeText(getApplicationContext(), "Received: message = " + message + " title = " + title ,Toast.LENGTH_LONG).show();
                sendNotification(message, title);

            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap.ic_launcher : R.mipmap.ic_launcher;
//        return 1;
    }




    private void sendNotification(String msg, String title) {



        if(title == null){
            title = getApplicationContext().getString(R.string.app_name);
        }


        ActivityManager am = (ActivityManager) GcmIntentService.this.getSystemService(ACTIVITY_SERVICE);
        // The first in the list of RunningTasks is always the foreground task.
        ActivityManager.RunningTaskInfo foregroundTaskInfo = am.getRunningTasks(1).get(0);


        String foregroundTaskPackageName = foregroundTaskInfo .topActivity.getPackageName();
//        PackageManager pm = GcmIntentService.this.getPackageManager();
//        PackageInfo foregroundAppPackageInfo = null;

        LOG.D(TAG,"sendNotification foregroundTaskPackageName = " + foregroundTaskPackageName);

//        try{
//            foregroundAppPackageInfo = pm.getPackageInfo(foregroundTaskPackageName, 0);
//        }catch(PackageManager.NameNotFoundException e){
//
//        }
//
//        LOG.D(TAG,"sendNotification foregroundAppPackageInfo = " + foregroundAppPackageInfo);
//        if(foregroundAppPackageInfo != null){
//            String foregroundTaskAppName = foregroundAppPackageInfo.applicationInfo.loadLabel(pm).toString();
//            LOG.D(TAG,"sendNotification foregroundTaskAppName = " + foregroundTaskAppName);
//        }

        LOG.D(TAG,"sendNotification getApplicationContext().getPackageName() = " + getApplicationContext().getPackageName());

//        if(foregroundTaskPackageName != null && foregroundTaskPackageName.equals(getApplicationContext().getPackageName())){
//
//            Intent intent = new Intent();
//            intent.setAction(Constants.EMERGENCY_ESCAPE);
//            intent.putExtra(Constants.EMERGENCY_TITLE, title);
//            intent.putExtra(Constants.EMERGENCY_MESSAGE, msg);
//            LocalBroadcastHelper.sendBroadcast(getApplicationContext(), intent);
//
//        }else{
//
//            final NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
//
//            Bundle messageBundle = new Bundle();
//
//            messageBundle.putBoolean(Constants.FROM_GCM, true);
//            messageBundle.putString(Constants.EMERGENCY_TITLE, title);
//            messageBundle.putString(Constants.EMERGENCY_MESSAGE, msg);
//
//            notificationIntent.putExtras(messageBundle);
//
//
//            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
//                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);
////		notificationIntent.putExtras(pageBundle);
//            PendingIntent intent = PendingIntent.getActivity(getApplicationContext(), 98,
//                    notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//
//
//            Notification.Builder builder = new Notification.Builder(getApplicationContext())
//                    .setSmallIcon(getNotificationIcon())
//                    .setContentTitle(title)
//                    .setContentText(msg)
//                    //.setStyle(new Notification.BigTextStyle().bigText(msg))
//                    .setContentIntent(intent);
//            if(android.os.Build.VERSION.SDK_INT >= 21) {
//                builder.setColor(getApplicationContext().getResources().getColor(R.color.footer_text_active));
//            }
//
//
//            Notification notification = builder.build();
//            notification.flags |= Notification.FLAG_AUTO_CANCEL;
//
//            notificationManager.notify(745, notification);
//        }


//        final NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
//
//        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
//                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
////		notificationIntent.putExtras(pageBundle);
//        PendingIntent intent = PendingIntent.getActivity(getApplicationContext(), 98,
//                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//
//
//        Notification.Builder builder = new Notification.Builder(getApplicationContext())
//                .setSmallIcon(getNotificationIcon())
//                .setContentTitle(title)
//                .setContentText(msg)
//                //.setStyle(new Notification.BigTextStyle().bigText(msg))
//                .setContentIntent(intent);
//        if(android.os.Build.VERSION.SDK_INT >= 21) {
//            builder.setColor(getApplicationContext().getResources().getColor(R.color.footer_text_active));
//        }
//
//
//        Notification notification = builder.build();
//        notification.flags |= Notification.FLAG_AUTO_CANCEL;
//
//        notificationManager.notify(745, notification);



    }
}
