package com.hack.purewater.android.dataModel;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by Giles on 2017/12/25.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class WaterRegisterModel {
    private int result = -1;
    private String text = null;
    private RoleInfoModel roleInfoModel = null;
    //
    @JsonProperty("result")
    public int getResult() {
        return result;
    }

    @JsonProperty("result")
    public void setResult(int result) {
        this.result = result;
    }

    @JsonProperty("text")
    public String getText() {
        return text;
    }

    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }


    @JsonProperty("role_info")
    public RoleInfoModel getUserInfoData() {
        return roleInfoModel;
    }
    @JsonProperty("role_info")
    public void setUserInfoData(RoleInfoModel roleInfoModel) {
        this.roleInfoModel = roleInfoModel;
    }

}
