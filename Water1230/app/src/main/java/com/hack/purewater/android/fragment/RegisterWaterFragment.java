package com.hack.purewater.android.fragment;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.hack.purewater.android.LandingPageActivity;
import com.hack.purewater.android.R;
import com.hack.purewater.android.dataModel.WaterRegisterModel;
import com.hack.purewater.android.util.Constants;
import com.hack.purewater.android.util.LOG;
import com.hack.purewater.android.util.apitask.listener.IWaterRegisterListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

/**
 * Created by Giles on 2017/12/24.
 */

public class RegisterWaterFragment extends BaseFragment {
    public static final String TAG = "RegisterWaterFragment";

    private View mView = null;
    private Context mContext = null;
    private LandingPageActivity mLandingPageActivity;
    private FragmentActivity mActivity = null;
    //    private NetworkManager mNetworkManager = null;
    private Boolean mCurrentNetworkAvaliable;

    private Button mBtnSubmit = null;
    private Button mBtnSelect = null;
    private RegisterWaterFragment mRegisterWaterFragment = null;
    private String personalImagePath = null;
    private EditText mtxtName = null;
    private EditText mtxtDob = null;
    private EditText mtxtEmail = null;
    private EditText mtxtAddress = null;
    private EditText mtxtPassword = null;
    private EditText mtxtconfirmPWD = null;
    private EditText mtxtPhone = null;
    private EditText mtxtWebsite = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LOG.D(TAG, "onCreate");
        mActivity = getActivity();
        mContext = getActivity();
        mLandingPageActivity = (LandingPageActivity) getActivity();


        Bundle bundle = getArguments();
        if (bundle != null) {
//            mPageFrom = bundle.getInt(IndoorMapFragment.KEY_PAGE_FROM, -1);
            bundle.clear();
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LOG.V(TAG, "[onCreateView]  mView = " + mView);

        if (mView == null) {
            mView = inflater.inflate(R.layout.register_water_fragment, null);
        } else {
            ViewGroup parent = (ViewGroup) mView.getParent();
            if (parent != null) {
                parent.removeView(mView);
            }
        }
        initView(mView);
        return mView;
    }

    @Override
    public void onStart() {
        super.onStart();
        LOG.D(TAG, "onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        LOG.D(TAG, "onResume");

//        mLandingPageActivity.setActionBarBackShow(false);
//        mLandingPageActivity.setActionBarShow(true);
//        mLandingPageActivity.setActionBarTitle(getString(R.string.txt_title_my));
//        mLandingPageActivity.setFooterBarShow(true);

    }

    @Override
    public void onStop() {
        super.onStop();

        LOG.V(TAG, "onStop() - start ");

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        LOG.V(TAG, "onDestroy() - start ");
    }

    private void initView(View view) {

        mBtnSubmit = (Button) mView.findViewById(R.id.btn_submit);
        mBtnSubmit.setOnClickListener(mBtnSubmitClickListener);
        this.mBtnSubmit.setEnabled(true);


        mBtnSelect = (Button) mView.findViewById(R.id.btn_select);
        mBtnSelect.setOnClickListener(mBtnSelectClickListener);

        mtxtName = (EditText) mView.findViewById(R.id.edt_register_name);
        mtxtDob = (EditText) mView.findViewById(R.id.edt_register_dob);
        mtxtEmail = (EditText) mView.findViewById(R.id.edt_email);
        mtxtAddress = (EditText) mView.findViewById(R.id.edt_register_address);
        mtxtPassword = (EditText) mView.findViewById(R.id.edt_register_password);
        mtxtconfirmPWD = (EditText) mView.findViewById(R.id.edt_register_confirm_pwd);
        mtxtPhone = (EditText) mView.findViewById(R.id.edt_register_telephone);
        mtxtWebsite = (EditText) mView.findViewById(R.id.edt_register_official_website);

        Spinner staticSpinner = (Spinner) mView.findViewById(R.id.static_spinner);

        // Create an ArrayAdapter using the string array and a default spinner
        ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter
                .createFromResource(getContext(), R.array.role_list,
                        android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        staticAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        staticSpinner.setAdapter(staticAdapter);

        staticSpinner.setOnItemSelectedListener(mSpinnerLisenter);
    }

    @Override
    public void onActivityResult(int reqCode, int resCode, Intent data) {
        if (resCode == Activity.RESULT_OK && data != null) {
            Uri selectedImageUri = data.getData();
            System.out.println(selectedImageUri.toString());
            // MEDIA GALLERY
            String selectedImagePath = ImageFilePath.getPath(
                    getActivity(), selectedImageUri);
            personalImagePath = selectedImagePath;
        }
    }

    private void launchFragment(int tag, boolean removeChooseList) {
        LOG.V(TAG, "[launchFragment] tag = " + tag);
        FragmentManager fm = getFragmentManager();
        //FragmentManager fm = getChildFragmentManager();
        if (fm == null) {
            LOG.W(TAG, "[launchFragment] FragmentManager is null.");
            return;
        }

        FragmentTransaction ft = fm.beginTransaction();
        if (ft == null) {
            LOG.W(TAG, "[launchFragment] FragmentTransaction is null.");
            return;
        }

        Bundle arguments = new Bundle();

        switch (tag) {

            case Constants.FRAGMENT_TAG_WATER_REGISTER:
                mRegisterWaterFragment = new RegisterWaterFragment();

//                arguments.putString(WebViewFragment.KEY_WEB_VIEW_URL, webViewUrl);
//                mHomeFragment.setArguments(arguments);

                ft.replace(Constants.LANDING_FRAME_ID, mRegisterWaterFragment, RegisterWaterFragment.TAG).commitAllowingStateLoss();
                break;


        }

    }

    private static final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");
    private static final MediaType MEDIA_TYPE_JPG = MediaType.parse("image/jpg");
    private Runnable mutiThread = new Runnable() {
        public void run() {
            JSONObject rold_info = new JSONObject();
            try {
                rold_info.put("name", mtxtName.getText().toString());
                rold_info.put("email", mtxtEmail.getText().toString());
                rold_info.put("DoB", mtxtDob.getText().toString());
                rold_info.put("telephone", mtxtPhone.getText().toString());
                rold_info.put("website", mtxtWebsite.getText().toString());
                rold_info.put("role", "inventor");
                rold_info.put("password", "robert001");
                rold_info.put("address", "robert001 address");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody requestBody = null;

            if (personalImagePath == null) {
                requestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("parm", rold_info.toString())
                        .build();
            } else {
                File imageFile = new File(personalImagePath);
                requestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("parm", rold_info.toString())
                        .addFormDataPart("personalImage", "logo-square.png",
                                RequestBody.create(MEDIA_TYPE_JPG, imageFile))
                        .addFormDataPart("IDImage", "logo-square.png",
                                RequestBody.create(MEDIA_TYPE_JPG, imageFile))
                        .build();

            }
            // Create a POST request to send the data to UPLOAD_URL
            Request request = new Request.Builder()
                    .url("http://123.193.224.62:8099/purewater/registration.php")
                    .post(requestBody)
                    .build();

// Execute the request and get the response from the server
            OkHttpClient okHttpClient = new OkHttpClient();

            try {
                Response response = okHttpClient.newCall(request).execute();
                if (response == null || !response.isSuccessful()) {
                    Log.w("Example", "Unable to upload to server.");
                } else {
                    String ResponseString = response.body().string();
                    try {

                        JSONObject obj = new JSONObject(ResponseString);

                        Log.d("My App", obj.toString());

                    } catch (Throwable t) {
                        Log.e("My App", "Could not parse malformed JSON: \"" + ResponseString + "\"");
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };
    private Button.OnClickListener mBtnSelectClickListener = new Button.OnClickListener() {
        public void onClick(View v) {
            LOG.D(TAG, "mBtnSelectClickListener");
            slectImage();
        }
    };
    private Button.OnClickListener mBtnSubmitClickListener = new Button.OnClickListener() {
        public void onClick(View v) {
            LOG.D(TAG, "mBtnSubmitClickListener");

            Thread thread = new Thread(mutiThread);
            thread.start();
        }
    };

    private Button.OnClickListener mBtnRegisterClickListener = new Button.OnClickListener() {
        public void onClick(View v) {
            LOG.D(TAG, "mBtnRegisterClickListener");
            //goto main activity
//            launchFragment();

        }
    };

    private IWaterRegisterListener waterRegisterListener = new IWaterRegisterListener() {
        @Override
        public void onWaterRegister(WaterRegisterModel model) {
            LOG.D(TAG, "model = " + model);
        }
    };

    private Spinner.OnItemSelectedListener mSpinnerLisenter= new Spinner.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view,
        int position, long id) {
            Log.v("item", (String) parent.getItemAtPosition(position));
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            // TODO Auto-generated method stub
        }
    };

    public void slectImage() {
        // 1. on Upload click call ACTION_GET_CONTENT intent
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        // 2. pick image only
        intent.setType("image/*");
        // 3. start activity
        startActivityForResult(intent, 0);

        // define onActivityResult to do something with picked image
    }


    public static class ImageFilePath {

        /**
         * Method for return file path of Gallery image
         *
         * @param context
         * @param uri
         * @return path of the selected image file from gallery
         */
        public static String getPath(final Context context, final Uri uri) {

            // check here to KITKAT or new version
            final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

            // DocumentProvider
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    if ("primary".equalsIgnoreCase(type)) {
                        return Environment.getExternalStorageDirectory() + "/"
                                + split[1];
                    }
                }
                // DownloadsProvider
                else if (isDownloadsDocument(uri)) {

                    final String id = DocumentsContract.getDocumentId(uri);
                    final Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"),
                            Long.valueOf(id));

                    return getDataColumn(context, contentUri, null, null);
                }
                // MediaProvider
                else if (isMediaDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }

                    final String selection = "_id=?";
                    final String[] selectionArgs = new String[]{split[1]};

                    return getDataColumn(context, contentUri, selection,
                            selectionArgs);
                }
            }
            // MediaStore (and general)
            else if ("content".equalsIgnoreCase(uri.getScheme())) {

                // Return the remote address
                if (isGooglePhotosUri(uri))
                    return uri.getLastPathSegment();

                return getDataColumn(context, uri, null, null);
            }
            // File
            else if ("file".equalsIgnoreCase(uri.getScheme())) {
                return uri.getPath();
            }

            return null;
        }

        /**
         * Get the value of the data column for this Uri. This is useful for
         * MediaStore Uris, and other file-based ContentProviders.
         *
         * @param context       The context.
         * @param uri           The Uri to query.
         * @param selection     (Optional) Filter used in the query.
         * @param selectionArgs (Optional) Selection arguments used in the query.
         * @return The value of the _data column, which is typically a file path.
         */
        public static String getDataColumn(Context context, Uri uri,
                                           String selection, String[] selectionArgs) {

            Cursor cursor = null;
            final String column = "_data";
            final String[] projection = {column};

            try {
                cursor = context.getContentResolver().query(uri, projection,
                        selection, selectionArgs, null);
                if (cursor != null && cursor.moveToFirst()) {
                    final int index = cursor.getColumnIndexOrThrow(column);
                    return cursor.getString(index);
                }
            } finally {
                if (cursor != null)
                    cursor.close();
            }
            return null;
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is ExternalStorageProvider.
         */
        public static boolean isExternalStorageDocument(Uri uri) {
            return "com.android.externalstorage.documents".equals(uri
                    .getAuthority());
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is DownloadsProvider.
         */
        public static boolean isDownloadsDocument(Uri uri) {
            return "com.android.providers.downloads.documents".equals(uri
                    .getAuthority());
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is MediaProvider.
         */
        public static boolean isMediaDocument(Uri uri) {
            return "com.android.providers.media.documents".equals(uri
                    .getAuthority());
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is Google Photos.
         */
        public static boolean isGooglePhotosUri(Uri uri) {
            return "com.google.android.apps.photos.content".equals(uri
                    .getAuthority());
        }
    }
}
